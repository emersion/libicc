# libicc

[ICC] v2 and v4 library.

## Building

libicc is built using [Meson]:

    meson setup build/
    ninja -C build/

[ICC]: https://www.color.org/icc_specs2.xalter
[Meson]: https://mesonbuild.com/

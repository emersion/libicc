#ifndef LIBICC_H
#define LIBICC_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define ICC_SIG(a, b, c, d) ((a << 24) | (b << 16) | (c << 8) | d)

enum icc_class {
	ICC_CLASS_INPUT_DEVICE = ICC_SIG('s', 'c', 'n', 'r'),
	ICC_CLASS_DISPLAY_DEVICE = ICC_SIG('m', 'n', 't', 'r'),
	ICC_CLASS_OUTPUT_DEVICE = ICC_SIG('p', 'r', 't', 'r'),

	ICC_CLASS_DEVICELINK = ICC_SIG('l', 'i', 'n', 'k'),
	ICC_CLASS_COLORSPACE_CONVERSION = ICC_SIG('s', 'p', 'a', 'c'),
	ICC_CLASS_ABSTRACT = ICC_SIG('a', 'b', 's', 't'),
	ICC_CLASS_NAMED_COLOR = ICC_SIG('n', 'm', 'c', 'l'),
};

enum icc_color_space {
	ICC_COLOR_SPACE_XYZ = ICC_SIG('X', 'Y', 'Z', ' '),
	ICC_COLOR_SPACE_LAB = ICC_SIG('L', 'a', 'b', ' '),
	ICC_COLOR_SPACE_LUV = ICC_SIG('L', 'u', 'v', ' '),
	ICC_COLOR_SPACE_YCBCR = ICC_SIG('Y', 'C', 'b', 'r'),
	ICC_COLOR_SPACE_YXY = ICC_SIG('Y', 'x', 'y', ' '),
	ICC_COLOR_SPACE_RGB = ICC_SIG('R', 'G', 'B', ' '),
	ICC_COLOR_SPACE_GRAY = ICC_SIG('G', 'R', 'A', 'Y'),
	ICC_COLOR_SPACE_HSV = ICC_SIG('H', 'S', 'V', ' '),
	ICC_COLOR_SPACE_HLS = ICC_SIG('H', 'L', 'S', ' '),
	ICC_COLOR_SPACE_CMYK = ICC_SIG('C', 'M', 'Y', 'K'),
	ICC_COLOR_SPACE_CMY = ICC_SIG('C', 'M', 'Y', ' '),
	ICC_COLOR_SPACE_2COLOR = ICC_SIG('2', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_3COLOR = ICC_SIG('3', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_4COLOR = ICC_SIG('4', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_5COLOR = ICC_SIG('5', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_6COLOR = ICC_SIG('6', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_7COLOR = ICC_SIG('7', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_8COLOR = ICC_SIG('8', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_9COLOR = ICC_SIG('9', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_10COLOR = ICC_SIG('A', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_11COLOR = ICC_SIG('B', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_12COLOR = ICC_SIG('C', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_13COLOR = ICC_SIG('D', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_14COLOR = ICC_SIG('E', 'C', 'L', 'R'),
	ICC_COLOR_SPACE_15COLOR = ICC_SIG('F', 'C', 'L', 'R'),
};

enum icc_pcs {
	ICC_PCS_XYZ = ICC_SIG('X', 'Y', 'Z', ' '),
	ICC_PCS_LAB = ICC_SIG('L', 'a', 'b', ' '),
};

enum icc_primary_platform {
	ICC_PRIMARY_PLATFORM_NONE = 0,
	ICC_PRIMARY_PLATFORM_APPLE = ICC_SIG('A', 'P', 'P', 'L'),
	ICC_PRIMARY_PLATFORM_MICROSOFT = ICC_SIG('M', 'S', 'F', 'T'),
	ICC_PRIMARY_PLATFORM_SILICON_GRAPHICS = ICC_SIG('S', 'G', 'I', ' '),
	ICC_PRIMARY_PLATFORM_SUN = ICC_SIG('S', 'U', 'N', 'W'),
	ICC_PRIMARY_PLATFORM_TALIGENT = ICC_SIG('T', 'G', 'N', 'T'),
};

enum icc_rendering_intent {
	ICC_RENDERING_INTENT_PERCEPTUAL = 0,
	ICC_RENDERING_INTENT_MEDIA_RELATIVE_COLORIMETRIC = 1,
	ICC_RENDERING_INTENT_SATURATION = 2,
	ICC_RENDERING_INTENT_ICC_ABSOLUTE_COLORIMETRIC = 3,
};

enum icc_tag {
	ICC_TAG_ATOB0 = ICC_SIG('A', '2', 'B', '0'),
	ICC_TAG_ATOB1 = ICC_SIG('A', '2', 'B', '1'),
	ICC_TAG_ATOB2 = ICC_SIG('A', '2', 'B', '2'),
	ICC_TAG_BLUE_MATRIX_COLUMN = ICC_SIG('b', 'X', 'Y', 'Z'),
	ICC_TAG_BLUE_TRC = ICC_SIG('b', 'T', 'R', 'C'),
	ICC_TAG_BTOA0 = ICC_SIG('B', '2', 'A', '0'),
	ICC_TAG_BTOA1 = ICC_SIG('B', '2', 'A', '1'),
	ICC_TAG_BTOA2 = ICC_SIG('B', '2', 'A', '2'),
	ICC_TAG_CALIBRATION_DATE_TIME = ICC_SIG('c', 'a', 'l', 't'),
	ICC_TAG_CHAR_TARGET = ICC_SIG('t', 'a', 'r', 'g'),
	ICC_TAG_CHROMATIC_ADAPTATION = ICC_SIG('c', 'h', 'a', 'd'),
	ICC_TAG_CHROMATICITY = ICC_SIG('c', 'h', 'r', 'm'),
	ICC_TAG_COPYRIGHT = ICC_SIG('c', 'p', 'r', 't'),
	ICC_TAG_CRD_INFO = ICC_SIG('c', 'r', 'd', 'i'),
	ICC_TAG_DEVICE_MFG_DESC = ICC_SIG('d', 'm', 'n', 'd'),
	ICC_TAG_DEVICE_MODEL_DESC = ICC_SIG('d', 'm', 'd', 'd'),
	ICC_TAG_DEVICE_SETTINGS = ICC_SIG('d', 'e', 'v', 's'),
	ICC_TAG_GAMUT = ICC_SIG('g', 'a', 'm', 't'),
	ICC_TAG_GRAY_TRC = ICC_SIG('k', 'T', 'R', 'C'),
	ICC_TAG_GREEN_MATRIX_COLUMN = ICC_SIG('g', 'X', 'Y', 'Z'),
	ICC_TAG_GREEN_TRC = ICC_SIG('g', 'T', 'R', 'C'),
	ICC_TAG_LUMINANCE = ICC_SIG('l', 'u', 'm', 'i'),
	ICC_TAG_MEASUREMENT = ICC_SIG('m', 'e', 'a', 's'),
	ICC_TAG_MEDIA_BLACK_POINT = ICC_SIG('b', 'k', 'p', 't'),
	ICC_TAG_MEDIA_WHITE_POINT = ICC_SIG('w', 't', 'p', 't'),
	ICC_TAG_NAMED_COLOR = ICC_SIG('n', 'c', 'o', 'l'),
	ICC_TAG_NAMED_COLOR2 = ICC_SIG('n', 'c', 'l', '2'),
	ICC_TAG_OUTPUT_RESPONSE = ICC_SIG('r', 'e', 's', 'p'),
	ICC_TAG_PREVIEW0 = ICC_SIG('p', 'r', 'e', '0'),
	ICC_TAG_PREVIEW1 = ICC_SIG('p', 'r', 'e', '1'),
	ICC_TAG_PREVIEW2 = ICC_SIG('p', 'r', 'e', '2'),
	ICC_TAG_PROFILE_DESCRIPTION = ICC_SIG('d', 'e', 's', 'c'),
	ICC_TAG_PROFILE_SEQUENCE_DESC = ICC_SIG('p', 's', 'e', 'q'),
	ICC_TAG_PS2_CRD0 = ICC_SIG('p', 's', 'd', '0'),
	ICC_TAG_PS2_CRD1 = ICC_SIG('p', 's', 'd', '1'),
	ICC_TAG_PS2_CRD2 = ICC_SIG('p', 's', 'd', '2'),
	ICC_TAG_PS2_CRD3 = ICC_SIG('p', 's', 'd', '3'),
	ICC_TAG_PS2_CSA = ICC_SIG('p', 's', '2', 's'),
	ICC_TAG_PS2_RENDERING_INTENT = ICC_SIG('p', 's', '2', 'i'),
	ICC_TAG_RED_MATRIX_COLUMN = ICC_SIG('r', 'X', 'Y', 'Z'),
	ICC_TAG_RED_TRC = ICC_SIG('r', 'T', 'R', 'C'),
	ICC_TAG_SCREENING_DESC = ICC_SIG('s', 'c', 'r', 'd'),
	ICC_TAG_SCREENING = ICC_SIG('s', 'c', 'r', 'n'),
	ICC_TAG_TECHNOLOGY = ICC_SIG('t', 'e', 'c', 'h'),
	ICC_TAG_UCRBG = ICC_SIG('b', 'f', 'd', ' '),
	ICC_TAG_VIEWING_COND_DESC = ICC_SIG('v', 'u', 'e', 'd'),
	ICC_TAG_VIEWING_CONDITIONS = ICC_SIG('v', 'i', 'e', 'w'),
	ICC_TAG_BTOD0 = ICC_SIG('B', '2', 'D', '0'),
	ICC_TAG_BTOD1 = ICC_SIG('B', '2', 'D', '1'),
	ICC_TAG_BTOD2 = ICC_SIG('B', '2', 'D', '2'),
	ICC_TAG_BTOD3 = ICC_SIG('B', '2', 'D', '3'),
	ICC_TAG_CICP = ICC_SIG('c', 'i', 'c', 'p'),
	ICC_TAG_COLORANT_ORDER = ICC_SIG('c', 'l', 'r', 'o'),
	ICC_TAG_COLORANT_TABLE = ICC_SIG('c', 'l', 'r', 't'),
	ICC_TAG_COLORANT_TABLE_OUT = ICC_SIG('c', 'l', 'o', 't'),
	ICC_TAG_COLORIMETRIC_INTENT_IMAGE_STATE = ICC_SIG('c', 'i', 'i', 's'),
	ICC_TAG_DTOB0 = ICC_SIG('D', '2', 'B', '0'),
	ICC_TAG_DTOB1 = ICC_SIG('D', '2', 'B', '1'),
	ICC_TAG_DTOB2 = ICC_SIG('D', '2', 'B', '2'),
	ICC_TAG_DTOB3 = ICC_SIG('D', '2', 'B', '3'),
	ICC_TAG_METADATA = ICC_SIG('m', 'e', 't', 'a'),
	ICC_TAG_PERCEPTUAL_RENDERINT_INTENT_GAMUT = ICC_SIG('r', 'i', 'g', '0'),
	ICC_TAG_PROFILE_SEQUENCE_IDENTIFIER = ICC_SIG('p', 's', 'i', 'd'),
	ICC_TAG_SATURATION_RENDERING_INTENT_GAMUT = ICC_SIG('r', 'i', 'g', '2'),
};

struct icc_date_time {
	uint16_t year;
	uint16_t month;
	uint16_t day;
	uint16_t hour;
	uint16_t minute;
	uint16_t second;
};

struct icc_s15_fixed16_number {
	int32_t i;
};

double
icc_s15_fixed16_number_to_double(struct icc_s15_fixed16_number num);

struct icc_u16_fixed16_number {
	uint32_t u;
};

double
icc_u16_fixed16_number_to_double(struct icc_u16_fixed16_number num);

struct icc_u8_fixed8_number {
	uint16_t u;
};

float
icc_u8_fixed8_number_to_float(struct icc_u8_fixed8_number num);

// TODO: consider renaming to icc_xyz_number?
struct icc_xyz {
	struct icc_s15_fixed16_number x, y, z;
};

struct icc_curve {
	uint32_t len;
	const uint16_t *values;
};

enum icc_parametric_curve_type {
	ICC_PARAMETRIC_CURVE_TYPE_0 = 0x00,
	ICC_PARAMETRIC_CURVE_TYPE_1 = 0x01,
	ICC_PARAMETRIC_CURVE_TYPE_2 = 0x02,
	ICC_PARAMETRIC_CURVE_TYPE_3 = 0x03,
	ICC_PARAMETRIC_CURVE_TYPE_4 = 0x04,
};

struct icc_parametric_curve {
	enum icc_parametric_curve_type type;
	struct icc_s15_fixed16_number g, a, b, c, d, e, f;
};

#define ICC_LUT8_NUM_TABLE_ENTRIES 256

struct icc_lut8 {
	uint8_t num_input_channels;
	uint8_t num_output_channels;
	uint8_t num_clut_grid_points;
	struct icc_s15_fixed16_number matrix[9];
	const uint8_t *input_table;
	const uint8_t *clut;
	const uint8_t *output_table;
};

struct icc_lut16 {
	uint8_t num_input_channels;
	uint8_t num_output_channels;
	uint8_t num_clut_grid_points;
	uint16_t num_input_table_entries;
	uint16_t num_output_table_entries;
	struct icc_s15_fixed16_number matrix[9];
	const uint16_t *input_table;
	const uint16_t *clut;
	const uint16_t *output_table;
};

enum icc_lut_ab_direction {
	ICC_LUT_AB_DIRECTION_ATOB,
	ICC_LUT_AB_DIRECTION_BTOA,
};

struct icc_lut_ab {
	enum icc_lut_ab_direction direction;
	uint8_t num_input_channels;
	uint8_t num_output_channels;

	uint8_t a_curves_len;
	const struct icc_lut_ab_curve *a_curves[16];

	uint8_t clut_num_grid_points[16];
	const uint8_t *clut8;
	const uint16_t *clut16;

	uint8_t m_curves_len;
	const struct icc_lut_ab_curve *m_curves[16];

	bool has_matrix;
	struct icc_s15_fixed16_number matrix_mult[9];
	struct icc_s15_fixed16_number matrix_add[3];

	uint8_t b_curves_len;
	const struct icc_lut_ab_curve *b_curves[16];
};

struct icc_lut_ab_curve {
	const struct icc_curve *curve;
	const struct icc_parametric_curve *parametric_curve;
};

enum icc_colorant_type {
	ICC_COLORANT_TYPE_UNKNOWN = 0,
	ICC_COLORANT_TYPE_BT_709 = 1,
	ICC_COLORANT_TYPE_SMPTE = 2,
	ICC_COLORANT_TYPE_EBU3213 = 3,
	ICC_COLORANT_TYPE_P22 = 4,
};

struct icc_chromaticity_xy {
	struct icc_u16_fixed16_number x, y;
};

struct icc_chromaticity {
	uint16_t num_device_channels;
	enum icc_colorant_type colorant_type;
	struct icc_chromaticity_xy ch1, ch2, ch3;
};

struct icc_chromatic_adaptation {
	struct icc_s15_fixed16_number matrix[9];
};

struct icc_dict {
	uint32_t len;
	const struct icc_dict_record *records;
};

struct icc_dict_record {
	const char *name;
	const char *value; // may be NULL
	const char *display_name; // may be NULL
	const char *display_value; // may be NULL
};

enum icc_ref_medium_gamut {
	ICC_REF_MEDIUM_GAMUT_PERCEPTUAL = ICC_SIG('p', 'r', 'm', 'g'),
};

struct icc_multi_process_transform {
	uint16_t num_input_channels;
	uint16_t num_output_channels;

	uint32_t len;
	const struct icc_multi_process_element *const *elements;
};

enum icc_multi_process_element_type {
	ICC_MULTI_PROCESS_ELEMENT_TYPE_CURVE_SET = ICC_SIG('c', 'v', 's', 't'),
	ICC_MULTI_PROCESS_ELEMENT_TYPE_MATRIX = ICC_SIG('m', 'a', 't', 'f'),
	ICC_MULTI_PROCESS_ELEMENT_TYPE_CLUT = ICC_SIG('c', 'l', 'u', 't'),
};

struct icc_multi_process_element {
	enum icc_multi_process_element_type type;
	uint16_t num_input_channels;
	uint16_t num_output_channels;

	// for ICC_MULTI_PROCESS_ELEMENT_TYPE_CURVE_SET, one curve per channel
	const struct icc_multi_process_curve *const *curves;
	// for ICC_MUTLI_PROCESS_ELEMENT_TYPE_MATRIX
	const float *matrix_mult, *matrix_add;
	// for ICC_MULTI_PROCESS_ELEMENT_TYPE_CLUT
	const struct icc_multi_process_clut *clut;
};

struct icc_multi_process_curve {
	uint16_t num_segments;
	const float *break_points;
	const struct icc_multi_process_curve_segment *const *segments;
};

enum icc_multi_process_curve_segment_type {
	ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_FORMULA = ICC_SIG('p', 'a', 'r', 'f'),
	ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_SAMPLED = ICC_SIG('s', 'a', 'm', 'f'),
};

struct icc_multi_process_curve_segment {
	enum icc_multi_process_curve_segment_type type;

	// for ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_FORMULA
	const struct icc_multi_process_curve_formula *formula;
	// for ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_SAMPLED
	const struct icc_multi_process_curve_sampled *sampled;
};

enum icc_multi_process_curve_formula_type {
	ICC_MULTI_PROCESS_CURVE_FORMULA_TYPE_0 = 0x00,
	ICC_MULTI_PROCESS_CURVE_FORMULA_TYPE_1 = 0x01,
	ICC_MULTI_PROCESS_CURVE_FORMULA_TYPE_2 = 0x02,
};

struct icc_multi_process_curve_formula {
	enum icc_multi_process_curve_formula_type type;
	float gamma, a, b, c, d, e;
};

struct icc_multi_process_curve_sampled {
	uint32_t len;
	const float *entries;
};

struct icc_multi_process_clut {
	uint8_t num_grid_points[16];
	const float *data_points;
};

struct icc_header {
	uint8_t major_version, minor_version, bugfix_version;
	enum icc_class class;
	enum icc_color_space color_space;
	enum icc_pcs pcs;
	struct icc_date_time creation_date_time;
	enum icc_primary_platform primary_platform;

	bool is_embedded;
	bool requires_embedded_color_data;

	enum icc_rendering_intent rendering_intent;
};

struct icc_profile;
struct icc_element;

struct icc_profile *
icc_profile_parse(FILE *f);

void
icc_profile_destroy(struct icc_profile *profile);

const struct icc_header *
icc_profile_get_header(const struct icc_profile *profile);

const struct icc_element *const *
icc_profile_get_elements(const struct icc_profile *profile);

enum icc_tag
icc_element_get_tag(const struct icc_element *elem);

const char *
icc_element_get_char_target(const struct icc_element *elem);

const char *
icc_element_get_copyright(const struct icc_element *elem);

const char *
icc_element_get_profile_description(const struct icc_element *elem);

const char *
icc_element_get_device_mfg_desc(const struct icc_element *elem);

const char *
icc_element_get_device_model_desc(const struct icc_element *elem);

const char *
icc_element_get_screening_desc(const struct icc_element *elem);

const char *
icc_element_get_viewing_cond_desc(const struct icc_element *elem);

struct icc_s15_fixed16_number
icc_element_get_luminance(const struct icc_element *elem);

struct icc_xyz
icc_element_get_media_black_point(const struct icc_element *elem);

struct icc_xyz
icc_element_get_media_white_point(const struct icc_element *elem);

struct icc_xyz
icc_element_get_red_colorant(const struct icc_element *elem);

struct icc_xyz
icc_element_get_green_colorant(const struct icc_element *elem);

struct icc_xyz
icc_element_get_blue_colorant(const struct icc_element *elem);

const struct icc_curve *
icc_element_get_curve(const struct icc_element *elem);

const struct icc_parametric_curve *
icc_element_get_parametric_curve(const struct icc_element *elem);

const struct icc_lut8 *
icc_element_get_lut8(const struct icc_element *elem);

const struct icc_lut16 *
icc_element_get_lut16(const struct icc_element *elem);

const struct icc_lut_ab *
icc_element_get_lut_ab(const struct icc_element *elem);

const struct icc_chromaticity *
icc_element_get_chromaticity(const struct icc_element *elem);

const struct icc_chromatic_adaptation *
icc_element_get_chromatic_adaptation(const struct icc_element *elem);

struct icc_dict
icc_element_get_metadata(const struct icc_element *elem);

enum icc_ref_medium_gamut
icc_element_get_perceptual_rendering_gamut(const struct icc_element *elem);

const struct icc_multi_process_transform *
icc_element_get_multi_process_transform(const struct icc_element *elem);

#endif

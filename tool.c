#include <getopt.h>
#include <libicc.h>
#include <stdlib.h>
#include <string.h>

static const char *
class_name(enum icc_class class)
{
	switch (class) {
	case ICC_CLASS_INPUT_DEVICE:
		return "input device";
	case ICC_CLASS_DISPLAY_DEVICE:
		return "display device";
	case ICC_CLASS_OUTPUT_DEVICE:
		return "output device";
	case ICC_CLASS_DEVICELINK:
		return "DeviceLink";
	case ICC_CLASS_COLORSPACE_CONVERSION:
		return "color space conversion";
	case ICC_CLASS_ABSTRACT:
		return "abstract";
	case ICC_CLASS_NAMED_COLOR:
		return "named color";
	}
	abort(); // unreachable
}

static const char *
color_space_name(enum icc_color_space color_space)
{
	switch (color_space) {
	case ICC_COLOR_SPACE_XYZ:
		return "XYZ";
	case ICC_COLOR_SPACE_LAB:
		return "LAB";
	case ICC_COLOR_SPACE_LUV:
		return "lUV";
	case ICC_COLOR_SPACE_YCBCR:
		return "YCbCr";
	case ICC_COLOR_SPACE_YXY:
		return "YXY";
	case ICC_COLOR_SPACE_RGB:
		return "RGB";
	case ICC_COLOR_SPACE_GRAY:
		return "Gray";
	case ICC_COLOR_SPACE_HSV:
		return "HSV";
	case ICC_COLOR_SPACE_HLS:
		return "HLS";
	case ICC_COLOR_SPACE_CMYK:
		return "CMYK";
	case ICC_COLOR_SPACE_CMY:
		return "CMY";
	case ICC_COLOR_SPACE_2COLOR:
		return "2color";
	case ICC_COLOR_SPACE_3COLOR:
		return "3color";
	case ICC_COLOR_SPACE_4COLOR:
		return "4color";
	case ICC_COLOR_SPACE_5COLOR:
		return "5color";
	case ICC_COLOR_SPACE_6COLOR:
		return "6color";
	case ICC_COLOR_SPACE_7COLOR:
		return "7color";
	case ICC_COLOR_SPACE_8COLOR:
		return "8color";
	case ICC_COLOR_SPACE_9COLOR:
		return "9color";
	case ICC_COLOR_SPACE_10COLOR:
		return "10color";
	case ICC_COLOR_SPACE_11COLOR:
		return "11color";
	case ICC_COLOR_SPACE_12COLOR:
		return "12color";
	case ICC_COLOR_SPACE_13COLOR:
		return "13color";
	case ICC_COLOR_SPACE_14COLOR:
		return "14color";
	case ICC_COLOR_SPACE_15COLOR:
		return "15color";
	}
	abort(); // unreachable
}

static const char *
pcs_name(enum icc_pcs pcs)
{
	switch (pcs) {
	case ICC_PCS_XYZ:
		return "XYZ";
	case ICC_PCS_LAB:
		return "LAB";
	}
	abort(); // unreachable
}

static const char *
rendering_intent_name(enum icc_rendering_intent intent)
{
	switch (intent) {
	case ICC_RENDERING_INTENT_PERCEPTUAL:
		return "perceptual";
	case ICC_RENDERING_INTENT_MEDIA_RELATIVE_COLORIMETRIC:
		return "media-relative colorimetric";
	case ICC_RENDERING_INTENT_SATURATION:
		return "saturation";
	case ICC_RENDERING_INTENT_ICC_ABSOLUTE_COLORIMETRIC:
		return "ICC-absolute colorimetric";
	}
	abort(); // unreachable
}

static const char *
tag_name(enum icc_tag tag, enum icc_class class)
{
	switch (tag) {
	case ICC_TAG_ATOB0:
		switch (class) {
		case ICC_CLASS_INPUT_DEVICE:
		case ICC_CLASS_DISPLAY_DEVICE:
		case ICC_CLASS_OUTPUT_DEVICE:
			return "Device to PCS (perceptual)";
		case ICC_CLASS_COLORSPACE_CONVERSION:
			return "Color space to PCS (perceptual)";
		case ICC_CLASS_ABSTRACT:
			return "PCS to PCS";
		case ICC_CLASS_DEVICELINK:
			return "Device 1 to device 2 (rendering)";
		default:
			return "A to B 0";
		}
	case ICC_TAG_ATOB1:
		switch (class) {
		case ICC_CLASS_INPUT_DEVICE:
		case ICC_CLASS_DISPLAY_DEVICE:
		case ICC_CLASS_OUTPUT_DEVICE:
			return "Device to PCS (colorimetric)";
		case ICC_CLASS_COLORSPACE_CONVERSION:
			return "Color space to PCS (colorimetric)";
		default:
			return "A to B 1";
		}
	case ICC_TAG_ATOB2:
		switch (class) {
		case ICC_CLASS_INPUT_DEVICE:
		case ICC_CLASS_DISPLAY_DEVICE:
		case ICC_CLASS_OUTPUT_DEVICE:
			return "Device to PCS (saturation)";
		case ICC_CLASS_COLORSPACE_CONVERSION:
			return "Color space to PCS (saturation)";
		default:
			return "A to B 2";
		}
	case ICC_TAG_BLUE_MATRIX_COLUMN:
		return "Blue matrix column";
	case ICC_TAG_BLUE_TRC:
		return "Blue TRC";
	case ICC_TAG_BTOA0:
		switch (class) {
		case ICC_CLASS_INPUT_DEVICE:
		case ICC_CLASS_DISPLAY_DEVICE:
		case ICC_CLASS_OUTPUT_DEVICE:
			return "PCS to device (perceptual)";
		case ICC_CLASS_COLORSPACE_CONVERSION:
			return "PCS to color space (perceptual)";
		default:
			return "B to A 0";
		}
	case ICC_TAG_BTOA1:
		switch (class) {
		case ICC_CLASS_INPUT_DEVICE:
		case ICC_CLASS_DISPLAY_DEVICE:
		case ICC_CLASS_OUTPUT_DEVICE:
			return "PCS to device (colorimetric)";
		case ICC_CLASS_COLORSPACE_CONVERSION:
			return "PCS to color space (colorimetric)";
		default:
			return "B to A 1";
		}
	case ICC_TAG_BTOA2:
		switch (class) {
		case ICC_CLASS_INPUT_DEVICE:
		case ICC_CLASS_DISPLAY_DEVICE:
		case ICC_CLASS_OUTPUT_DEVICE:
			return "PCS to device (saturation)";
		case ICC_CLASS_COLORSPACE_CONVERSION:
			return "PCS to color space (saturation)";
		default:
			return "B to A 2";
		}
	case ICC_TAG_CALIBRATION_DATE_TIME:
		return "Calibration date";
	case ICC_TAG_CHAR_TARGET:
		return "Characterization target";
	case ICC_TAG_CHROMATIC_ADAPTATION:
		return "Chromatic adaptation";
	case ICC_TAG_CHROMATICITY:
		return "Chromaticity";
	case ICC_TAG_COPYRIGHT:
		return "Copyright";
	case ICC_TAG_CRD_INFO:
		return "CRD info";
	case ICC_TAG_DEVICE_MFG_DESC:
		return "Device manufacturer description";
	case ICC_TAG_DEVICE_MODEL_DESC:
		return "Device model description";
	case ICC_TAG_DEVICE_SETTINGS:
		return "Device settings";
	case ICC_TAG_GAMUT:
		return "Gamut";
	case ICC_TAG_GRAY_TRC:
		return "Gray TRC";
	case ICC_TAG_GREEN_MATRIX_COLUMN:
		return "Green matrix column";
	case ICC_TAG_GREEN_TRC:
		return "Green TRC";
	case ICC_TAG_LUMINANCE:
		return "Luminance";
	case ICC_TAG_MEASUREMENT:
		return "Measurement";
	case ICC_TAG_MEDIA_BLACK_POINT:
		return "Media black point";
	case ICC_TAG_MEDIA_WHITE_POINT:
		return "Media white point";
	case ICC_TAG_NAMED_COLOR:
		return "Named color";
	case ICC_TAG_NAMED_COLOR2:
		return "Named color (2)";
	case ICC_TAG_OUTPUT_RESPONSE:
		return "Output response";
	case ICC_TAG_PREVIEW0:
		return "Preview transformation 0";
	case ICC_TAG_PREVIEW1:
		return "Preview transformation 1";
	case ICC_TAG_PREVIEW2:
		return "Preview transformation 2";
	case ICC_TAG_PROFILE_DESCRIPTION:
		return "Profile description";
	case ICC_TAG_PROFILE_SEQUENCE_DESC:
		return "Profile sequence description";
	case ICC_TAG_PS2_CRD0:
		return "PostScript level 2 color rendering dictionary (perceptual)";
	case ICC_TAG_PS2_CRD1:
		return "PostScript level 2 color rendering dictionary (colorimetric)";
	case ICC_TAG_PS2_CRD2:
		return "PostScript level 2 color rendering dictionary (saturation)";
	case ICC_TAG_PS2_CRD3:
		return "PostScript level 2 color rendering dictionary (ICC-absolute)";
	case ICC_TAG_PS2_CSA:
		return "PostScript level 2 color space array";
	case ICC_TAG_PS2_RENDERING_INTENT:
		return "PostScript level 2 rendering intent";
	case ICC_TAG_RED_MATRIX_COLUMN:
		return "Red matrix column";
	case ICC_TAG_RED_TRC:
		return "Red TRC";
	case ICC_TAG_SCREENING_DESC:
		return "Screening description";
	case ICC_TAG_SCREENING:
		return "Screening";
	case ICC_TAG_TECHNOLOGY:
		return "Technology";
	case ICC_TAG_UCRBG:
		return "Under color removal and black generation";
	case ICC_TAG_VIEWING_COND_DESC:
		return "Viewing conditions description";
	case ICC_TAG_VIEWING_CONDITIONS:
		return "Viewing conditions";
	case ICC_TAG_BTOD0:
		return "PCS to device (perceptual, float32)";
	case ICC_TAG_BTOD1:
		return "PCS to device (colorimetric, float32)";
	case ICC_TAG_BTOD2:
		return "PCS to device (saturation, float32)";
	case ICC_TAG_BTOD3:
		return "PCS to device (ICC-absolute, float32)";
	case ICC_TAG_CICP:
		return "CICP";
	case ICC_TAG_COLORANT_ORDER:
		return "Colorant order";
	case ICC_TAG_COLORANT_TABLE:
		return "Colorant table";
	case ICC_TAG_COLORANT_TABLE_OUT:
		return "Colorant table out";
	case ICC_TAG_COLORIMETRIC_INTENT_IMAGE_STATE:
		return "Colorimetric intent image state";
	case ICC_TAG_DTOB0:
		return "Device to PCS (perceptual, float32)";
	case ICC_TAG_DTOB1:
		return "Device to PCS (colorimetric, float32)";
	case ICC_TAG_DTOB2:
		return "Device to PCS (saturation, float32)";
	case ICC_TAG_DTOB3:
		return "Device to PCS (ICC-absolute, float32)";
	case ICC_TAG_METADATA:
		return "Metadata";
	case ICC_TAG_PERCEPTUAL_RENDERINT_INTENT_GAMUT:
		return "Perceptual rendering intent gamut";
	case ICC_TAG_PROFILE_SEQUENCE_IDENTIFIER:
		return "Profile sequence identifier";
	case ICC_TAG_SATURATION_RENDERING_INTENT_GAMUT:
		return "Saturation rendering intent gamut";
	}
	abort(); // unreachable
}

static void
print_xyz(struct icc_xyz num)
{
	double x = icc_s15_fixed16_number_to_double(num.x);
	double y = icc_s15_fixed16_number_to_double(num.y);
	double z = icc_s15_fixed16_number_to_double(num.z);
	printf("XYZ(%f, %f, %f)", x, y, z);
}

static void
print_curve(struct icc_curve curve)
{
	printf("curve (");
	switch (curve.len) {
	case 0:
		printf("identity");
		break;
	case 1:;
		struct icc_u8_fixed8_number num = { curve.values[0] };
		printf("gamma %f", icc_u8_fixed8_number_to_float(num));
		break;
	default:
		printf("%u entries", curve.len);
		break;
	}
	printf(")\n");
}

static void
print_parametric_curve(const struct icc_parametric_curve *param_curve, const char *indent)
{
	double g = icc_s15_fixed16_number_to_double(param_curve->g);
	double a = icc_s15_fixed16_number_to_double(param_curve->a);
	double b = icc_s15_fixed16_number_to_double(param_curve->b);
	double c = icc_s15_fixed16_number_to_double(param_curve->c);
	double d = icc_s15_fixed16_number_to_double(param_curve->d);
	double e = icc_s15_fixed16_number_to_double(param_curve->e);

	printf("parametric curve\n");
	switch (param_curve->type) {
	case ICC_PARAMETRIC_CURVE_TYPE_0:
		printf("%s  Y = Xᵍ\n", indent);
		printf("%s  g = %f\n", indent, g);
		break;
	case ICC_PARAMETRIC_CURVE_TYPE_1:
		printf("%s  Y = (aX + b)ᵍ   (X ≥ -b/a)\n", indent);
		printf("%s  Y = 0           (X < -b/a)\n", indent);
		printf("%s  g = %f\n", indent, g);
		printf("%s  a = %f\n", indent, a);
		printf("%s  b = %f\n", indent, b);
		break;
	case ICC_PARAMETRIC_CURVE_TYPE_2:
		printf("%s  Y = (aX + b)ᵍ + c   (X ≥ -b/a)\n", indent);
		printf("%s  Y = c               (X < -b/a)\n", indent);
		printf("%s  g = %f\n", indent, g);
		printf("%s  a = %f\n", indent, a);
		printf("%s  b = %f\n", indent, b);
		printf("%s  c = %f\n", indent, c);
		break;
	case ICC_PARAMETRIC_CURVE_TYPE_3:
		printf("%s  Y = (aX + b)ᵍ   (X ≥ d)\n", indent);
		printf("%s  Y = cX          (X < d)\n", indent);
		printf("%s  g = %f\n", indent, g);
		printf("%s  a = %f\n", indent, a);
		printf("%s  b = %f\n", indent, b);
		printf("%s  c = %f\n", indent, c);
		printf("%s  d = %f\n", indent, d);
		break;
	case ICC_PARAMETRIC_CURVE_TYPE_4:
		printf("%s  Y = (aX + b)ᵍ + e   (X ≥ d)\n", indent);
		printf("%s  Y = cX + f          (X < d)\n", indent);
		printf("%s  g = %f\n", indent, g);
		printf("%s  a = %f\n", indent, a);
		printf("%s  b = %f\n", indent, b);
		printf("%s  c = %f\n", indent, c);
		printf("%s  d = %f\n", indent, d);
		printf("%s  e = %f\n", indent, e);
		break;
	}
}

static bool
matrix_is_identity(const struct icc_s15_fixed16_number matrix[static 9])
{
	const int32_t identity[] = {
		0x10000, 0, 0,
		0, 0x10000, 0,
		0, 0, 0x10000,
	};
	return memcmp(matrix, identity, sizeof(identity)) == 0;
}

static void
print_matrix(const struct icc_s15_fixed16_number matrix[static 9], const char *indent)
{
	if (matrix_is_identity(matrix)) {
		printf(" identity\n");
	} else {
		printf("\n");
		for (size_t i = 0; i < 3; i++) {
			double a = icc_s15_fixed16_number_to_double(matrix[i * 3 + 0]);
			double b = icc_s15_fixed16_number_to_double(matrix[i * 3 + 1]);
			double c = icc_s15_fixed16_number_to_double(matrix[i * 3 + 2]);
			printf("%s%f %f %f\n", indent, a, b, c);
		}
	}
}

static void
print_lut8(const struct icc_lut8 *lut8)
{
	printf("    Matrix:");
	print_matrix(lut8->matrix, "      ");

	printf("    Input table: %u channels, %u entries\n",
		lut8->num_input_channels, ICC_LUT8_NUM_TABLE_ENTRIES);
	printf("    CLUT: %u grid points\n", lut8->num_clut_grid_points);
	printf("    Output table: %u channels, %u entries\n",
		lut8->num_output_channels, ICC_LUT8_NUM_TABLE_ENTRIES);
}

static void
print_lut16(const struct icc_lut16 *lut16)
{
	printf("    Matrix:");
	print_matrix(lut16->matrix, "      ");

	printf("    Input table: %u channels, %u entries\n",
		lut16->num_input_channels, lut16->num_input_table_entries);
	printf("    CLUT: %u grid points\n", lut16->num_clut_grid_points);
	printf("    Output table: %u channels, %u entries\n",
		lut16->num_output_channels, lut16->num_output_table_entries);
}

static const char *
lut_ab_direction_name(enum icc_lut_ab_direction direction)
{
	switch (direction) {
	case ICC_LUT_AB_DIRECTION_ATOB:
		return "lutAToB";
	case ICC_LUT_AB_DIRECTION_BTOA:
		return "lutBToA";
	}
	abort(); // unreachable
}

static void
print_lut_ab_curve_array(const struct icc_lut_ab_curve *const *curves, size_t len)
{
	for (size_t i = 0; i < len; i++) {
		const struct icc_lut_ab_curve *ab_curve = curves[i];
		printf("      Curve for channel %zu: ", i);
		if (ab_curve->curve != NULL) {
			print_curve(*ab_curve->curve);
		} else if (ab_curve->parametric_curve != NULL) {
			print_parametric_curve(ab_curve->parametric_curve, "      ");
		} else {
			printf("\n");
		}
	}
}

static bool
lut_ab_matrix_is_identity(const struct icc_lut_ab *lut_ab)
{
	const struct icc_s15_fixed16_number null_add[3] = {0};
	return matrix_is_identity(lut_ab->matrix_mult) &&
		memcmp(lut_ab->matrix_add, null_add, sizeof(lut_ab->matrix_add)) == 0;
}

static void
print_lut_ab(const struct icc_lut_ab *lut_ab)
{
	printf("    Number of input channels: %u\n", lut_ab->num_input_channels);
	printf("    Number of output channels: %u\n", lut_ab->num_output_channels);

	if (lut_ab->a_curves_len > 0) {
		printf("    A curve:\n");
		print_lut_ab_curve_array(lut_ab->a_curves, lut_ab->a_curves_len);
	}

	if (lut_ab->clut8 != NULL || lut_ab->clut16 != NULL) {
		printf("    CLUT (");
		for (size_t i = 0; lut_ab->clut_num_grid_points[i] != 0; i++) {
			if (i > 0) {
				printf(" × ");
			}
			printf("%u", lut_ab->clut_num_grid_points[i]);
		}
		printf(" grid points)\n");
	}

	if (lut_ab->m_curves_len > 0) {
		printf("    M curve:\n");
		print_lut_ab_curve_array(lut_ab->m_curves, lut_ab->m_curves_len);
	}

	if (lut_ab->has_matrix) {
		printf("    Matrix:");
		if (lut_ab_matrix_is_identity(lut_ab)) {
			printf(" identity\n");
		} else {
			printf("\n");
			for (size_t i = 0; i < 3; i++) {
				double v1 = icc_s15_fixed16_number_to_double(lut_ab->matrix_mult[i * 3 + 0]);
				double v2 = icc_s15_fixed16_number_to_double(lut_ab->matrix_mult[i * 3 + 1]);
				double v3 = icc_s15_fixed16_number_to_double(lut_ab->matrix_mult[i * 3 + 2]);
				double v4 = icc_s15_fixed16_number_to_double(lut_ab->matrix_add[i]);
				printf("      %f %f %f | %f\n", v1, v2, v3, v4);
			}
		}
	}

	if (lut_ab->b_curves_len > 0) {
		printf("    B curve:\n");
		print_lut_ab_curve_array(lut_ab->b_curves, lut_ab->b_curves_len);
	}
}

static const char *
colorant_type_name(enum icc_colorant_type type)
{
	switch (type) {
	case ICC_COLORANT_TYPE_UNKNOWN:
		return "unknown";
	case ICC_COLORANT_TYPE_BT_709:
		return "ITU-R BT.709";
	case ICC_COLORANT_TYPE_SMPTE:
		return "SMPTE RP145-1994";
	case ICC_COLORANT_TYPE_EBU3213:
		return "EBU Tech.3213-E";
	case ICC_COLORANT_TYPE_P22:
		return "P22";
	}
	abort(); // unreachable
}

static void
print_chromaticity(const struct icc_chromaticity *chromaticity)
{
	printf("    Number of device channels: %u\n", chromaticity->num_device_channels);
	printf("    Phosphor/colorant type: %s\n", colorant_type_name(chromaticity->colorant_type));

	const struct icc_chromaticity_xy channels[] = {
		chromaticity->ch1,
		chromaticity->ch2,
		chromaticity->ch3,
	};
	for (size_t i = 0; i < sizeof(channels) / sizeof(channels[0]); i++) {
		printf("    Channel %zu coordinates: XY(%f, %f)\n", i + 1,
			icc_u16_fixed16_number_to_double(channels[i].x),
			icc_u16_fixed16_number_to_double(channels[i].y));
	}
}

static void
print_chromatic_adaptation(const struct icc_chromatic_adaptation *chrom_adapt)
{
	print_matrix(chrom_adapt->matrix, "    ");
}

static void
print_dict(struct icc_dict dict)
{
	for (size_t i = 0; i < dict.len; i++) {
		const struct icc_dict_record *record = &dict.records[i];
		printf("    %s", record->name);
		if (record->display_name != NULL) {
			printf(" (%s)", record->display_name);
		}
		printf(" = ");
		if (record->value == NULL) {
			printf("<unset>");
		} else if (record->value[0] == '\0') {
			printf("<empty>");
		} else {
			printf("%s", record->value);
		}
		if (record->display_value != NULL) {
			printf(" (%s)", record->display_value);
		}
		printf("\n");
	}
}

static const char *
ref_medium_gamut_name(enum icc_ref_medium_gamut rmg)
{
	switch (rmg) {
	case ICC_REF_MEDIUM_GAMUT_PERCEPTUAL:
		return "perceptual";
	}
	abort(); // unreachable
}

static const char *
multi_process_curve_segment_type_name(enum icc_multi_process_curve_segment_type type)
{
	switch (type) {
	case ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_FORMULA:
		return "formula";
	case ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_SAMPLED:
		return "sampled";
	}
	abort(); // unreachable
}

static void
print_multi_process_curve_segment(const struct icc_multi_process_curve_segment *segment)
{
	printf("%s", multi_process_curve_segment_type_name(segment->type));
	switch (segment->type) {
	case ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_FORMULA:
		printf("\n");
		switch (segment->formula->type) {
		case ICC_MULTI_PROCESS_CURVE_FORMULA_TYPE_0:
			printf("          Y = (aX + b)ᵞ + c\n");
			printf("          γ = %f\n", segment->formula->gamma);
			printf("          a = %f\n", segment->formula->a);
			printf("          b = %f\n", segment->formula->b);
			printf("          c = %f\n", segment->formula->c);
			break;
		case ICC_MULTI_PROCESS_CURVE_FORMULA_TYPE_1:
			printf("          Y = a log₁₀(bXᵞ + c) + d\n");
			printf("          γ = %f\n", segment->formula->gamma);
			printf("          a = %f\n", segment->formula->a);
			printf("          b = %f\n", segment->formula->b);
			printf("          c = %f\n", segment->formula->c);
			printf("          d = %f\n", segment->formula->d);
			break;
		case ICC_MULTI_PROCESS_CURVE_FORMULA_TYPE_2:
			printf("          Y = abᶜˣ⁺ᵈ + e\n");
			printf("          a = %f\n", segment->formula->a);
			printf("          b = %f\n", segment->formula->b);
			printf("          c = %f\n", segment->formula->c);
			printf("          d = %f\n", segment->formula->d);
			printf("          e = %f\n", segment->formula->e);
			break;
		}
		break;
	case ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_SAMPLED:
		printf(" (%u entries)\n", segment->sampled->len);
		break;
	}
}

static void
print_multi_process_curve(const struct icc_multi_process_curve *curve)
{
	for (size_t i = 0; i < curve->num_segments; i++) {
		printf("        Segment ]");
		if (i == 0) {
			printf("-∞");
		} else {
			printf("%f", curve->break_points[i - 1]);
		}
		printf(", ");
		if (i == (size_t) curve->num_segments - 1) {
			printf("+∞[");
		} else {
			printf("%f]", curve->break_points[i]);
		}
		printf(": ");
		print_multi_process_curve_segment(curve->segments[i]);
	}
}

static const char *
multi_process_element_type_name(enum icc_multi_process_element_type type)
{
	switch (type) {
	case ICC_MULTI_PROCESS_ELEMENT_TYPE_CURVE_SET:
		return "Curve set";
	case ICC_MULTI_PROCESS_ELEMENT_TYPE_MATRIX:
		return "Matrix";
	case ICC_MULTI_PROCESS_ELEMENT_TYPE_CLUT:
		return "CLUT";
	}
	abort(); // unreachable
}

static void
print_multi_process_element(const struct icc_multi_process_element *elem)
{
	printf("    %s (%u input channels, %u output channels",
		multi_process_element_type_name(elem->type),
		elem->num_input_channels, elem->num_output_channels);

	switch (elem->type) {
	case ICC_MULTI_PROCESS_ELEMENT_TYPE_CURVE_SET:
		printf("):\n");
		for (size_t i = 0; i < elem->num_input_channels; i++) {
			printf("      Channel %zu curve:\n", i);
			print_multi_process_curve(elem->curves[i]);
		}
		break;
	case ICC_MULTI_PROCESS_ELEMENT_TYPE_MATRIX:
		printf(")\n");
		break;
	case ICC_MULTI_PROCESS_ELEMENT_TYPE_CLUT:
		printf(", ");
		for (size_t i = 0; i < elem->num_input_channels; i++) {
			if (i > 0) {
				printf(" × ");
			}
			printf("%u", elem->clut->num_grid_points[i]);
		}
		printf(" grid points)\n");
		break;
	}
}

static void
print_multi_process_transform(const struct icc_multi_process_transform *transform)
{
	printf("multi-process transform (%u input channels, %u output channels)\n",
		transform->num_input_channels, transform->num_output_channels);

	for (size_t i = 0; i < transform->len; i++) {
		print_multi_process_element(transform->elements[i]);
	}
}

static void
print_element(const struct icc_element *elem, enum icc_class class)
{
	printf("  %s", tag_name(icc_element_get_tag(elem), class));
	switch (icc_element_get_tag(elem)) {
	case ICC_TAG_CHAR_TARGET:
		printf(": %zu bytes", strlen(icc_element_get_char_target(elem)));
		break;
	case ICC_TAG_COPYRIGHT:
		printf(": %s", icc_element_get_copyright(elem));
		break;
	case ICC_TAG_PROFILE_DESCRIPTION:
		printf(": %s", icc_element_get_profile_description(elem));
		break;
	case ICC_TAG_DEVICE_MFG_DESC:
		printf(": %s", icc_element_get_device_mfg_desc(elem));
		break;
	case ICC_TAG_DEVICE_MODEL_DESC:
		printf(": %s", icc_element_get_device_model_desc(elem));
		break;
	case ICC_TAG_SCREENING_DESC:
		printf(": %s", icc_element_get_screening_desc(elem));
		break;
	case ICC_TAG_VIEWING_COND_DESC:
		printf(": %s", icc_element_get_viewing_cond_desc(elem));
		break;
	case ICC_TAG_LUMINANCE:
		printf(": %f cd/m²",
			icc_s15_fixed16_number_to_double(icc_element_get_luminance(elem)));
		break;
	case ICC_TAG_MEDIA_BLACK_POINT:
		printf(": ");
		print_xyz(icc_element_get_media_black_point(elem));
		break;
	case ICC_TAG_MEDIA_WHITE_POINT:
		printf(": ");
		print_xyz(icc_element_get_media_white_point(elem));
		break;
	case ICC_TAG_RED_MATRIX_COLUMN:
		printf(": ");
		print_xyz(icc_element_get_red_colorant(elem));
		break;
	case ICC_TAG_GREEN_MATRIX_COLUMN:
		printf(": ");
		print_xyz(icc_element_get_green_colorant(elem));
		break;
	case ICC_TAG_BLUE_MATRIX_COLUMN:
		printf(": ");
		print_xyz(icc_element_get_blue_colorant(elem));
		break;
	case ICC_TAG_RED_TRC:
	case ICC_TAG_GREEN_TRC:
	case ICC_TAG_BLUE_TRC:
	case ICC_TAG_GRAY_TRC:;
		const struct icc_curve *curve = icc_element_get_curve(elem);
		const struct icc_parametric_curve *param_curve = icc_element_get_parametric_curve(elem);
		if (curve != NULL) {
			printf(": ");
			print_curve(*curve);
		} else if (param_curve != NULL) {
			printf(": ");
			print_parametric_curve(param_curve, "  ");
		} else {
			printf("\n");
		}
		return;
	case ICC_TAG_ATOB0:
	case ICC_TAG_ATOB1:
	case ICC_TAG_ATOB2:
	case ICC_TAG_BTOA0:
	case ICC_TAG_BTOA1:
	case ICC_TAG_BTOA2:
	case ICC_TAG_GAMUT:
	case ICC_TAG_PREVIEW0:
	case ICC_TAG_PREVIEW1:
	case ICC_TAG_PREVIEW2:;
		const struct icc_lut8 *lut8 = icc_element_get_lut8(elem);
		const struct icc_lut16 *lut16 = icc_element_get_lut16(elem);
		const struct icc_lut_ab *lut_ab = icc_element_get_lut_ab(elem);
		if (lut8 != NULL) {
			printf(": lut8\n");
			print_lut8(lut8);
		} else if (lut16 != NULL) {
			printf(": lut16\n");
			print_lut16(lut16);
		} else if (lut_ab != NULL) {
			printf(": %s\n", lut_ab_direction_name(lut_ab->direction));
			print_lut_ab(lut_ab);
		} else {
			printf("\n");
		}
		return;
	case ICC_TAG_CHROMATICITY:
		printf(":\n");
		print_chromaticity(icc_element_get_chromaticity(elem));
		return;
	case ICC_TAG_CHROMATIC_ADAPTATION:
		printf(":");
		print_chromatic_adaptation(icc_element_get_chromatic_adaptation(elem));
		return;
	case ICC_TAG_METADATA:
		printf(":\n");
		print_dict(icc_element_get_metadata(elem));
		return;
	case ICC_TAG_PERCEPTUAL_RENDERINT_INTENT_GAMUT:
		printf(": %s\n", ref_medium_gamut_name(icc_element_get_perceptual_rendering_gamut(elem)));
		return;
	case ICC_TAG_BTOD0:
	case ICC_TAG_BTOD1:
	case ICC_TAG_BTOD2:
	case ICC_TAG_BTOD3:
	case ICC_TAG_DTOB0:
	case ICC_TAG_DTOB1:
	case ICC_TAG_DTOB2:
	case ICC_TAG_DTOB3:
		printf(": ");
		print_multi_process_transform(icc_element_get_multi_process_transform(elem));
		return;
	case ICC_TAG_CALIBRATION_DATE_TIME:
	case ICC_TAG_CRD_INFO:
	case ICC_TAG_DEVICE_SETTINGS:
	case ICC_TAG_MEASUREMENT:
	case ICC_TAG_NAMED_COLOR:
	case ICC_TAG_NAMED_COLOR2:
	case ICC_TAG_OUTPUT_RESPONSE:
	case ICC_TAG_PROFILE_SEQUENCE_DESC:
	case ICC_TAG_PS2_CRD0:
	case ICC_TAG_PS2_CRD1:
	case ICC_TAG_PS2_CRD2:
	case ICC_TAG_PS2_CRD3:
	case ICC_TAG_PS2_CSA:
	case ICC_TAG_PS2_RENDERING_INTENT:
	case ICC_TAG_SCREENING:
	case ICC_TAG_TECHNOLOGY:
	case ICC_TAG_UCRBG:
	case ICC_TAG_VIEWING_CONDITIONS:
	case ICC_TAG_CICP:
	case ICC_TAG_COLORANT_ORDER:
	case ICC_TAG_COLORANT_TABLE:
	case ICC_TAG_COLORANT_TABLE_OUT:
	case ICC_TAG_COLORIMETRIC_INTENT_IMAGE_STATE:
	case ICC_TAG_PROFILE_SEQUENCE_IDENTIFIER:
	case ICC_TAG_SATURATION_RENDERING_INTENT_GAMUT:
		break; // TODO
	}
	printf("\n");
}

static void
print_date_time(const struct icc_date_time *dt)
{
	printf("%u-%02u-%02u %02u:%02u:%02u", dt->year, dt->month, dt->day,
		dt->hour, dt->minute, dt->second);
}

int
main(int argc, char **argv) {
	int opt;
	while ((opt = getopt(argc, argv, "h")) != -1) {
		fprintf(stderr, "Usage: icc-decode [options...] [in]\n");
		return opt == 'h' ? 0 : 1;
	}

	FILE *in = stdin;
	if (optind < argc) {
		in = fopen(argv[optind], "r");
		if (in == NULL) {
			perror("Failed to open input file");
			return 1;
		}
	}

	struct icc_profile *profile = icc_profile_parse(in);
	if (profile == NULL) {
		fprintf(stderr, "Failed to parse ICC profile\n");
		return 1;
	}

	fclose(in);

	const struct icc_header *header = icc_profile_get_header(profile);
	printf("Version: %d.%d.%d\n", header->major_version, header->minor_version, header->bugfix_version);
	printf("Class: %s\n", class_name(header->class));
	printf("Color space: %s\n", color_space_name(header->color_space));
	printf("Profile connection space: %s\n", pcs_name(header->pcs));
	printf("Created at: ");
	print_date_time(&header->creation_date_time);
	printf("\n");
	printf("Embedded profile: %s\n", header->is_embedded ? "yes" : "no");
	printf("Profile can%s be used independently from the embedded color data\n",
		header->requires_embedded_color_data ? "not" : "");
	printf("Rendering intent: %s\n", rendering_intent_name(header->rendering_intent));

	printf("Tags:\n");
	const struct icc_element *const *elems = icc_profile_get_elements(profile);
	for (size_t i = 0; elems[i] != NULL; i++) {
		print_element(elems[i], header->class);
	}

	icc_profile_destroy(profile);
	return 0;
}

#include <assert.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "libicc.h"

#define HEADER_SIZE 128
#define TAG_ENTRY_SIZE 12
#define S15_FIXED16_NUMBER_SIZE 4
#define FLOAT32_NUMBER_SIZE 4
#define XYZ_NUMBER_SIZE (3 * S15_FIXED16_NUMBER_SIZE)

struct icc_profile {
	size_t size;
	struct icc_header header;

	size_t tag_table_size;
	struct icc_element *elements;
	struct icc_element **element_ptrs;
};

struct icc_multi_process_transform_priv {
	struct icc_multi_process_transform base;
	struct icc_multi_process_element_priv *elements;
};

struct icc_lut_ab_curve_priv {
	struct icc_lut_ab_curve base;
	struct icc_curve curve;
	struct icc_parametric_curve parametric_curve;
};

struct icc_lut_ab_priv {
	struct icc_lut_ab base;
	struct icc_lut_ab_curve_priv a_curves[16];
	struct icc_lut_ab_curve_priv m_curves[16];
	struct icc_lut_ab_curve_priv b_curves[16];
};

struct icc_element {
	enum icc_tag tag;
	uint32_t offset, size;

	uint32_t type;
	char *text;
	char *text_description;
	struct icc_xyz xyz;
	struct icc_curve curve;
	struct icc_parametric_curve parametric_curve;
	struct icc_lut8 lut8;
	struct icc_lut16 lut16;
	struct icc_lut_ab_priv lut_ab;
	struct icc_chromaticity chromaticity;
	struct icc_chromatic_adaptation chromatic_adaptation;
	struct icc_dict metadata;
	enum icc_ref_medium_gamut perceptual_rendering_gamut;
	struct icc_multi_process_transform_priv multi_process_transform;
};

struct icc_multi_process_element_priv {
	struct icc_multi_process_element base;
	struct icc_multi_process_curve_priv *curves;
	struct icc_multi_process_clut clut;
};

struct icc_multi_process_curve_priv {
	struct icc_multi_process_curve base;
	struct icc_multi_process_curve_segment_priv *segments;
};

struct icc_multi_process_curve_segment_priv {
	struct icc_multi_process_curve_segment base;
	struct icc_multi_process_curve_formula formula;
	struct icc_multi_process_curve_sampled sampled;
};

enum icc_type {
	ICC_TYPE_CHROMATICITY = ICC_SIG('c', 'h', 'r', 'm'),
	ICC_TYPE_CRD_INFO = ICC_SIG('c', 'r', 'd', 'i'),
	ICC_TYPE_CURVE = ICC_SIG('c', 'u', 'r', 'v'),
	ICC_TYPE_DATA = ICC_SIG('d', 'a', 't', 'a'),
	ICC_TYPE_DATE_TIME = ICC_SIG('d', 't', 'i', 'm'),
	ICC_TYPE_DEVICE_SETTINGS = ICC_SIG('d', 'e', 'v', 's'),
	ICC_TYPE_LUT16 = ICC_SIG('m', 'f', 't', '2'),
	ICC_TYPE_LUT8 = ICC_SIG('m', 'f', 't', '1'),
	ICC_TYPE_MEASUREMENT = ICC_SIG('m', 'e', 'a', 's'),
	ICC_TYPE_NAMED_COLOR = ICC_SIG('n', 'c', 'o', 'l'),
	ICC_TYPE_NAMED_COLOR2 = ICC_SIG('n', 'c', 'l', '2'),
	ICC_TYPE_PROFILE_SEQUENCE_DESC = ICC_SIG('p', 's', 'e', 'q'),
	ICC_TYPE_RESPONSE_CURVE_SET16 = ICC_SIG('r', 'c', 's', '2'),
	ICC_TYPE_S15_FIXED16_ARRAY = ICC_SIG('s', 'f', '3', '2'),
	ICC_TYPE_SCREENING = ICC_SIG('s', 'c', 'r', 'n'),
	ICC_TYPE_SIGNATURE = ICC_SIG('s', 'i', 'g', ' '),
	ICC_TYPE_TEXT_DESCRIPTION = ICC_SIG('d', 'e', 's', 'c'),
	ICC_TYPE_TEXT = ICC_SIG('t', 'e', 'x', 't'),
	ICC_TYPE_U16_FIXED16_ARRAY = ICC_SIG('u', 'f', '3', '2'),
	ICC_TYPE_UCRBG = ICC_SIG('b', 'f', 'd', ' '),
	ICC_TYPE_UINT16_ARRAY = ICC_SIG('u', 'i', '1', '6'),
	ICC_TYPE_UINT32_ARRAY = ICC_SIG('u', 'i', '3', '2'),
	ICC_TYPE_UINT64_ARRAY = ICC_SIG('u', 'i', '6', '4'),
	ICC_TYPE_UINT8_ARRAY = ICC_SIG('u', 'i', '0', '8'),
	ICC_TYPE_VIEWING_CONDITIONS = ICC_SIG('v', 'i', 'e', 'w'),
	ICC_TYPE_XYZ = ICC_SIG('X', 'Y', 'Z', ' '),
	ICC_TYPE_CICP = ICC_SIG('c', 'i', 'c', 'p'),
	ICC_TYPE_COLORANT_ORDER = ICC_SIG('c', 'l', 'r', 'o'),
	ICC_TYPE_COLORANT_TABLE = ICC_SIG('c', 'l', 'r', 't'),
	ICC_TYPE_DICT = ICC_SIG('d', 'i', 'c', 't'),
	ICC_TYPE_LUT_ATOB = ICC_SIG('m', 'A', 'B', ' '),
	ICC_TYPE_LUT_BTOA = ICC_SIG('m', 'B', 'A', ' '),
	ICC_TYPE_MULTI_LOCALIZED_UNICODE = ICC_SIG('m', 'l', 'u', 'c'),
	ICC_TYPE_MULTI_PROCESS_ELEMENTS = ICC_SIG('m', 'p', 'e', 't'),
	ICC_TYPE_PARAMETRIC_CURVE = ICC_SIG('p', 'a', 'r', 'a'),
	ICC_TYPE_PROFILE_SEQUENCE_IDENTIFIER = ICC_SIG('p', 's', 'i', 'd'),
};

double
icc_s15_fixed16_number_to_double(struct icc_s15_fixed16_number num)
{
	return (double) num.i / 0x10000;
}

double
icc_u16_fixed16_number_to_double(struct icc_u16_fixed16_number num)
{
	return (double) num.u / 0x10000;
}

float
icc_u8_fixed8_number_to_float(struct icc_u8_fixed8_number num)
{
	return (float) num.u / 0x100;
}

static bool
read_full(FILE *f, uint8_t *dst, size_t dst_size)
{
	size_t size = 0;
	while (size < dst_size) {
		size_t n = fread(&dst[size], 1, dst_size - size, f);
		if (n == 0) {
			return false;
		}
		size += n;
	}
	return true;
}

static uint16_t
parse_u16(const uint8_t raw[static 2])
{
	return ((uint16_t) raw[0] << 8) | (uint16_t) raw[1];
}

static uint32_t
parse_u32(const uint8_t raw[static 4])
{
	return ((uint32_t) raw[0] << 24) | ((uint32_t) raw[1] << 16) |
		((uint32_t) raw[2] << 8) | (uint32_t) raw[3];
}

static float
parse_float32(const uint8_t raw[static 4])
{
#if !defined(__STDC_IEC_60559_BFP__) && !defined(__STDC_IEC_559__)
#error "IEEE 754 floats are not supported"
#endif

	union {
		uint32_t u;
		float f;
	} v;
	v.u = parse_u32(raw);
	return v.f;
}

static void
sig_str(char str[static 5], uint32_t sig)
{
	str[0] = (char)((sig >> 24) & 0xFF);
	str[1] = (char)((sig >> 16) & 0xFF);
	str[2] = (char)((sig >> 8) & 0xFF);
	str[3] = (char)((sig >> 0) & 0xFF);
	str[4] = '\0';
}

static void
parse_date_time(struct icc_date_time *dt, const uint8_t raw[static 12])
{
	*dt = (struct icc_date_time) {
		.year = parse_u16(&raw[0]),
		.month = parse_u16(&raw[2]),
		.day = parse_u16(&raw[4]),
		.hour = parse_u16(&raw[6]),
		.minute = parse_u16(&raw[8]),
		.second = parse_u16(&raw[10]),
	};
}

static bool
read_header(struct icc_profile *profile, FILE *f)
{
	struct icc_header *header = &profile->header;

	uint8_t raw[HEADER_SIZE];
	if (!read_full(f, raw, sizeof(raw))) {
		return false;
	}

	profile->size = parse_u32(&raw[0]);
	if (profile->size < HEADER_SIZE) {
		fprintf(stderr, "invalid profile size (%zu smaller than header size)\n",
			profile->size);
		return false;
	}

	header->major_version = raw[8];
	header->minor_version = (raw[9] >> 4) & 0xF;
	header->bugfix_version = (raw[9] >> 0) & 0xF;
	if (header->major_version != 2 && header->major_version != 4) {
		fprintf(stderr, "unsupported ICC version %d\n", header->major_version);
		return false;
	}

	uint32_t raw_class = parse_u32(&raw[12]);
	switch (raw_class) {
	case ICC_CLASS_INPUT_DEVICE:
	case ICC_CLASS_DISPLAY_DEVICE:
	case ICC_CLASS_OUTPUT_DEVICE:
	case ICC_CLASS_DEVICELINK:
	case ICC_CLASS_COLORSPACE_CONVERSION:
	case ICC_CLASS_ABSTRACT:
	case ICC_CLASS_NAMED_COLOR:
		header->class = raw_class;
		break;
	default:;
		char class_str[5];
		sig_str(class_str, raw_class);
		fprintf(stderr, "invalid profile class '%s'\n", class_str);
		return false;
	}

	uint32_t raw_color_space = parse_u32(&raw[16]);
	switch (raw_color_space) {
	case ICC_COLOR_SPACE_XYZ:
	case ICC_COLOR_SPACE_LAB:
	case ICC_COLOR_SPACE_LUV:
	case ICC_COLOR_SPACE_YCBCR:
	case ICC_COLOR_SPACE_YXY:
	case ICC_COLOR_SPACE_RGB:
	case ICC_COLOR_SPACE_GRAY:
	case ICC_COLOR_SPACE_HSV:
	case ICC_COLOR_SPACE_HLS:
	case ICC_COLOR_SPACE_CMYK:
	case ICC_COLOR_SPACE_CMY:
	case ICC_COLOR_SPACE_2COLOR:
	case ICC_COLOR_SPACE_3COLOR:
	case ICC_COLOR_SPACE_4COLOR:
	case ICC_COLOR_SPACE_5COLOR:
	case ICC_COLOR_SPACE_6COLOR:
	case ICC_COLOR_SPACE_7COLOR:
	case ICC_COLOR_SPACE_8COLOR:
	case ICC_COLOR_SPACE_9COLOR:
	case ICC_COLOR_SPACE_10COLOR:
	case ICC_COLOR_SPACE_11COLOR:
	case ICC_COLOR_SPACE_12COLOR:
	case ICC_COLOR_SPACE_13COLOR:
	case ICC_COLOR_SPACE_14COLOR:
	case ICC_COLOR_SPACE_15COLOR:
		header->color_space = raw_color_space;
		break;
	default:;
		char color_space_str[5];
		sig_str(color_space_str, raw_color_space);
		fprintf(stderr, "invalid profile color space '%s'\n", color_space_str);
		return false;
	}

	uint32_t raw_pcs = parse_u32(&raw[20]);
	switch (raw_pcs) {
	case ICC_PCS_XYZ:
	case ICC_PCS_LAB:
		header->pcs = raw_pcs;
		break;
	default:;
		char pcs_str[5];
		sig_str(pcs_str, raw_pcs);
		fprintf(stderr, "invalid profile PCS '%s'\n", pcs_str);
		return false;
	}

	parse_date_time(&header->creation_date_time, &raw[24]);

	uint32_t file_sig = parse_u32(&raw[36]);
	if (file_sig != ICC_SIG('a', 'c', 's', 'p')) {
		fprintf(stderr, "invalid ICC profile file signature\n");
		return false;
	}

	uint32_t raw_primary_platform = parse_u32(&raw[40]);
	switch (raw_primary_platform) {
	case ICC_PRIMARY_PLATFORM_NONE:
	case ICC_PRIMARY_PLATFORM_APPLE:
	case ICC_PRIMARY_PLATFORM_MICROSOFT:
	case ICC_PRIMARY_PLATFORM_SILICON_GRAPHICS:
	case ICC_PRIMARY_PLATFORM_SUN:
	case ICC_PRIMARY_PLATFORM_TALIGENT:
		header->primary_platform = raw_primary_platform;
		break;
	}

	uint32_t flags = parse_u32(&raw[44]);
	header->is_embedded = flags & (1 << 0);
	header->requires_embedded_color_data = flags & (1 << 1);

	uint32_t raw_rendering_intent = parse_u32(&raw[64]);
	switch (raw_rendering_intent) {
	case ICC_RENDERING_INTENT_PERCEPTUAL:
	case ICC_RENDERING_INTENT_MEDIA_RELATIVE_COLORIMETRIC:
	case ICC_RENDERING_INTENT_SATURATION:
	case ICC_RENDERING_INTENT_ICC_ABSOLUTE_COLORIMETRIC:
		header->rendering_intent = raw_rendering_intent;
		break;
	default:
		fprintf(stderr, "invalid rendering intent %u\n", raw_rendering_intent);
		return false;
	}

	return true;
}

static bool
check_tag(uint32_t tag)
{
	switch (tag) {
	case ICC_TAG_ATOB0:
	case ICC_TAG_ATOB1:
	case ICC_TAG_ATOB2:
	case ICC_TAG_BLUE_MATRIX_COLUMN:
	case ICC_TAG_BLUE_TRC:
	case ICC_TAG_BTOA0:
	case ICC_TAG_BTOA1:
	case ICC_TAG_BTOA2:
	case ICC_TAG_CALIBRATION_DATE_TIME:
	case ICC_TAG_CHAR_TARGET:
	case ICC_TAG_CHROMATIC_ADAPTATION:
	case ICC_TAG_CHROMATICITY:
	case ICC_TAG_COPYRIGHT:
	case ICC_TAG_CRD_INFO:
	case ICC_TAG_DEVICE_MFG_DESC:
	case ICC_TAG_DEVICE_MODEL_DESC:
	case ICC_TAG_DEVICE_SETTINGS:
	case ICC_TAG_GAMUT:
	case ICC_TAG_GRAY_TRC:
	case ICC_TAG_GREEN_MATRIX_COLUMN:
	case ICC_TAG_GREEN_TRC:
	case ICC_TAG_LUMINANCE:
	case ICC_TAG_MEASUREMENT:
	case ICC_TAG_MEDIA_BLACK_POINT:
	case ICC_TAG_MEDIA_WHITE_POINT:
	case ICC_TAG_NAMED_COLOR:
	case ICC_TAG_NAMED_COLOR2:
	case ICC_TAG_OUTPUT_RESPONSE:
	case ICC_TAG_PREVIEW0:
	case ICC_TAG_PREVIEW1:
	case ICC_TAG_PREVIEW2:
	case ICC_TAG_PROFILE_DESCRIPTION:
	case ICC_TAG_PROFILE_SEQUENCE_DESC:
	case ICC_TAG_PS2_CRD0:
	case ICC_TAG_PS2_CRD1:
	case ICC_TAG_PS2_CRD2:
	case ICC_TAG_PS2_CRD3:
	case ICC_TAG_PS2_CSA:
	case ICC_TAG_PS2_RENDERING_INTENT:
	case ICC_TAG_RED_MATRIX_COLUMN:
	case ICC_TAG_RED_TRC:
	case ICC_TAG_SCREENING_DESC:
	case ICC_TAG_SCREENING:
	case ICC_TAG_TECHNOLOGY:
	case ICC_TAG_UCRBG:
	case ICC_TAG_VIEWING_COND_DESC:
	case ICC_TAG_VIEWING_CONDITIONS:
	case ICC_TAG_BTOD0:
	case ICC_TAG_BTOD1:
	case ICC_TAG_BTOD2:
	case ICC_TAG_BTOD3:
	case ICC_TAG_CICP:
	case ICC_TAG_COLORANT_ORDER:
	case ICC_TAG_COLORANT_TABLE:
	case ICC_TAG_COLORANT_TABLE_OUT:
	case ICC_TAG_COLORIMETRIC_INTENT_IMAGE_STATE:
	case ICC_TAG_DTOB0:
	case ICC_TAG_DTOB1:
	case ICC_TAG_DTOB2:
	case ICC_TAG_DTOB3:
	case ICC_TAG_METADATA:
	case ICC_TAG_PERCEPTUAL_RENDERINT_INTENT_GAMUT:
	case ICC_TAG_PROFILE_SEQUENCE_IDENTIFIER:
	case ICC_TAG_SATURATION_RENDERING_INTENT_GAMUT:
		return true;
	default:
		return false;
	}
}

static bool
read_tag_table(struct icc_profile *profile, FILE *f)
{
	uint8_t raw_tag_count[4];
	if (!read_full(f, raw_tag_count, sizeof(raw_tag_count))) {
		return false;
	}
	uint32_t tag_count = parse_u32(raw_tag_count);

	profile->tag_table_size = 4 + tag_count * TAG_ENTRY_SIZE;
	if (profile->size < HEADER_SIZE + profile->tag_table_size) {
		fprintf(stderr, "invalid profile size (%zu smaller than table size)\n",
			profile->size);
		return false;
	}

	profile->elements = calloc(tag_count, sizeof(profile->elements[0]));
	if (profile->elements == NULL) {
		return false;
	}
	profile->element_ptrs = calloc(tag_count + 1, sizeof(profile->element_ptrs[0]));
	if (profile->element_ptrs == NULL) {
		return false;
	}

	size_t elements_len = 0;
	for (size_t i = 0; i < tag_count; i++) {
		struct icc_element *elem = &profile->elements[elements_len];

		uint8_t raw_entry[TAG_ENTRY_SIZE];
		if (!read_full(f, raw_entry, sizeof(raw_entry))) {
			return false;
		}

		uint32_t tag = parse_u32(&raw_entry[0]);
		if (!check_tag(tag)) {
			continue;
		}

		elem->tag = tag;
		elem->offset = parse_u32(&raw_entry[4]);
		elem->size = parse_u32(&raw_entry[8]);

		if (elem->size == 0 || elem->offset + elem->size > profile->size || elem->offset < HEADER_SIZE + profile->tag_table_size) {
			fprintf(stderr, "invalid element offset/size\n");
			return false;
		}

		profile->element_ptrs[elements_len] = elem;
		elements_len++;
	}

	return true;
}

static bool
expect_type(uint32_t got, enum icc_type want)
{
	if (got == want) {
		return true;
	}
	char got_str[5], want_str[5];
	sig_str(got_str, got);
	sig_str(want_str, want);
	fprintf(stderr, "invalid type: got '%s', want '%s'\n", got_str, want_str);
	return false;
}

static bool
expect_size(size_t got, size_t want, const char *type)
{
	if (got >= want) {
		return true;
	}
	fprintf(stderr, "invalid %s element data: got %zu bytes, expected at least %zu\n",
		type, got, want);
	return false;
}

static char *
parse_text(uint32_t type, const uint8_t *raw, size_t size)
{
	if (!expect_type(type, ICC_TYPE_TEXT) || !expect_size(size, 8, "text")) {
		return NULL;
	}
	return strndup((char *) &raw[8], size - 8);
}

static char *
parse_text_description(uint32_t type, const uint8_t *raw, size_t size)
{
	if (!expect_type(type, ICC_TYPE_TEXT_DESCRIPTION) ||
			!expect_size(size, 12, "text description")) {
		return NULL;
	}

	uint32_t ascii_size = parse_u32(&raw[8]);
	if (ascii_size > 12 + size) {
		fprintf(stderr, "invalid ASCII text description size\n");
		return NULL;
	}

	// TODO: Unicode
	return strndup((char *) &raw[12], ascii_size);
}

static bool
parse_sig(uint32_t *out, uint32_t type, const uint8_t *raw, size_t size)
{
	if (!expect_type(type, ICC_TYPE_SIGNATURE) ||
			!expect_size(size, 12, "signature")) {
		return false;
	}
	*out = parse_u32(&raw[8]);
	return true;
}

static char *
decode_utf16(const uint8_t *raw, size_t size)
{
	// Convert UTF-16 to UTF-8
	char *utf8_str = malloc(size + 1);
	if (utf8_str == NULL) {
		return NULL;
	}

	size_t j = 0;
	for (size_t i = 0; i < size; i += sizeof(uint16_t)) {
		uint16_t ch = parse_u16(&raw[i]);
		if (ch < 0x80) {
			utf8_str[j++] = (char) ch;
		} else {
			utf8_str[j++] = (char) ((ch >> 6) | 0xC0);
			utf8_str[j++] = (char) ((ch & 0x3F) | 0x80);
		}
	}
	utf8_str[j] = '\0';

	return utf8_str;
}

static char *
parse_multi_localized_unicode(uint32_t type, const uint8_t *raw, size_t size)
{
	if (!expect_type(type, ICC_TYPE_MULTI_LOCALIZED_UNICODE) ||
			!expect_size(size, 28, "multi-localized unicode")) {
		return NULL;
	}

	// TODO: parse all records
	uint32_t record_size = parse_u32(&raw[20]);
	uint32_t record_offset = parse_u32(&raw[24]);
	if (record_offset + record_size > size) {
		fprintf(stderr, "invalid multi-localized unicode record size");
		return NULL;
	}

	return decode_utf16(&raw[record_offset], record_size);
}

static struct icc_s15_fixed16_number
parse_s15_fixed16_number(const uint8_t raw[static S15_FIXED16_NUMBER_SIZE])
{
	return (struct icc_s15_fixed16_number) { (int32_t) parse_u32(raw) };
}

static struct icc_xyz
parse_xyz_number(const uint8_t raw[static XYZ_NUMBER_SIZE])
{
	return (struct icc_xyz) {
		.x = parse_s15_fixed16_number(&raw[0]),
		.y = parse_s15_fixed16_number(&raw[4]),
		.z = parse_s15_fixed16_number(&raw[8]),
	};
}

static bool
parse_xyz(struct icc_xyz *out, uint32_t type, const uint8_t *raw, size_t size)
{
	if (!expect_type(type, ICC_TYPE_XYZ) || !expect_size(size, 8, "xyz")) {
		return false;
	}

	size_t values_size = size - 8;
	if (values_size % XYZ_NUMBER_SIZE != 0) {
		fprintf(stderr, "invalid xyz size\n");
		return false;
	}
	size_t values_len = values_size / XYZ_NUMBER_SIZE;

	// TODO: read all
	if (values_len != 1) {
		fprintf(stderr, "expected a single xyz entry\n");
		return false;
	}

	*out = parse_xyz_number(&raw[8]);
	return true;
}

static ssize_t
parse_curve(struct icc_curve *out, const uint8_t *raw, size_t size)
{
	if (!expect_size(size, 12, "curve")) {
		return -1;
	}

	uint32_t values_len = parse_u32(&raw[8]);
	size_t values_size = values_len * sizeof(uint16_t);
	size_t total_size = 12 + values_size;
	if (size < total_size) {
		fprintf(stderr, "invalid number of values in curve\n");
		return -1;
	}

	uint16_t *values = NULL;
	if (values_len > 0) {
		values = malloc(values_size);
		if (values == NULL) {
			return -1;
		}
	}

	for (size_t i = 0; i < values_len; i++) {
		values[i] = parse_u16(&raw[12 + i * sizeof(uint16_t)]);
	}

	*out = (struct icc_curve) {
		.len = values_len,
		.values = values,
	};
	return (ssize_t) total_size;
}

static ssize_t
parse_parametric_curve(struct icc_parametric_curve *out, const uint8_t *raw, size_t size)
{
	if (!expect_size(size, 12, "parametric curve")) {
		return -1;
	}

	uint16_t type = parse_u16(&raw[8]);
	uint32_t params_len;
	switch (type) {
	case ICC_PARAMETRIC_CURVE_TYPE_0:
		params_len = 1;
		break;
	case ICC_PARAMETRIC_CURVE_TYPE_1:
		params_len = 3;
		break;
	case ICC_PARAMETRIC_CURVE_TYPE_2:
		params_len = 4;
		break;
	case ICC_PARAMETRIC_CURVE_TYPE_3:
		params_len = 5;
		break;
	case ICC_PARAMETRIC_CURVE_TYPE_4:
		params_len = 7;
		break;
	default:
		fprintf(stderr, "invalid parametric curve type 0x%"PRIX16"\n", type);
		return -1;
	}

	size_t total_size = 12 + S15_FIXED16_NUMBER_SIZE * params_len;
	if (!expect_size(size, total_size, "parametric curve")) {
		return -1;
	}

	struct icc_parametric_curve param_curve = {
		.type = type,
	};

	struct icc_s15_fixed16_number *params[] = {
		&param_curve.g,
		&param_curve.a,
		&param_curve.b,
		&param_curve.c,
		&param_curve.d,
		&param_curve.e,
		&param_curve.f,
	};
	assert(params_len <= sizeof(params) / sizeof(params[0]));

	for (size_t i = 0; i < params_len; i++) {
		*params[i] = parse_s15_fixed16_number(&raw[12 + S15_FIXED16_NUMBER_SIZE * i]);
	}

	*out = param_curve;
	return (ssize_t) total_size;
}

static bool
parse_lut8(struct icc_lut8 *out, const uint8_t *raw, size_t size)
{
	if (!expect_size(size, 48, "lut8")) {
		return false;
	}

	struct icc_lut8 lut8 = {
		.num_input_channels = raw[8],
		.num_output_channels = raw[9],
		.num_clut_grid_points = raw[10],
	};

	if (lut8.num_input_channels > 4 || lut8.num_output_channels > 4) {
		fprintf(stderr, "invalid number of lut8 channels\n");
		return false;
	}

	for (size_t i = 0; i < sizeof(lut8.matrix) / sizeof(lut8.matrix[0]); i++) {
		lut8.matrix[i] = parse_s15_fixed16_number(&raw[12 + i * S15_FIXED16_NUMBER_SIZE]);
	}

	size_t input_table_len = lut8.num_input_channels * ICC_LUT8_NUM_TABLE_ENTRIES;
	size_t output_table_len = lut8.num_output_channels * ICC_LUT8_NUM_TABLE_ENTRIES;
	size_t input_table_size = input_table_len * sizeof(uint8_t);
	size_t output_table_size = output_table_len * sizeof(uint8_t);

	size_t clut_len = 1;
	for (size_t i = 0; i < lut8.num_input_channels; i++) {
		clut_len *= lut8.num_clut_grid_points;
	}
	clut_len *= lut8.num_output_channels;
	size_t clut_size = clut_len * sizeof(uint8_t);

	if (!expect_size(size, 48 + input_table_size + clut_size + output_table_size, "lut8")) {
		return false;
	}

	uint8_t *input_table = malloc(input_table_size);
	uint8_t *output_table = malloc(output_table_size);
	uint8_t *clut = malloc(clut_size);
	if (input_table == NULL || output_table == NULL || clut == NULL) {
		free(input_table);
		free(output_table);
		free(clut);
		return false;
	}

	const uint8_t *raw_input_table = &raw[48];
	memcpy(input_table, raw_input_table, input_table_size);

	const uint8_t *raw_clut = &raw_input_table[input_table_size];
	memcpy(clut, raw_clut, clut_size);

	const uint8_t *raw_output_table = &raw_clut[clut_size];
	memcpy(output_table, raw_output_table, output_table_size);

	lut8.input_table = input_table;
	lut8.output_table = output_table;
	lut8.clut = clut;

	*out = lut8;
	return true;
}

static bool
parse_lut16(struct icc_lut16 *out, const uint8_t *raw, size_t size)
{
	if (!expect_size(size, 52, "lut16")) {
		return false;
	}

	struct icc_lut16 lut16 = {
		.num_input_channels = raw[8],
		.num_output_channels = raw[9],
		.num_clut_grid_points = raw[10],
	};

	if (lut16.num_input_channels > 4 || lut16.num_output_channels > 4) {
		fprintf(stderr, "invalid number of lut16 channels\n");
		return false;
	}

	for (size_t i = 0; i < sizeof(lut16.matrix) / sizeof(lut16.matrix[0]); i++) {
		lut16.matrix[i] = parse_s15_fixed16_number(&raw[12 + i * S15_FIXED16_NUMBER_SIZE]);
	}

	lut16.num_input_table_entries = parse_u16(&raw[48]);
	lut16.num_output_table_entries = parse_u16(&raw[50]);

	if (lut16.num_input_table_entries < 2 || lut16.num_input_table_entries > 4096) {
		fprintf(stderr, "invalid number of entries in input table\n");
		return false;
	}
	if (lut16.num_output_table_entries < 2 || lut16.num_output_table_entries > 4096) {
		fprintf(stderr, "invalid number of entries in output table\n");
		return false;
	}

	size_t input_table_len = lut16.num_input_channels * lut16.num_input_table_entries;
	size_t output_table_len = lut16.num_output_channels * lut16.num_output_table_entries;
	size_t input_table_size = input_table_len * sizeof(uint16_t);
	size_t output_table_size = output_table_len * sizeof(uint16_t);

	size_t clut_len = 1;
	for (size_t i = 0; i < lut16.num_input_channels; i++) {
		clut_len *= lut16.num_clut_grid_points;
	}
	clut_len *= lut16.num_output_channels;
	size_t clut_size = clut_len * sizeof(uint16_t);

	if (!expect_size(size, 52 + input_table_size + clut_size + output_table_size, "lut16")) {
		return false;
	}

	uint16_t *input_table = malloc(input_table_size);
	uint16_t *output_table = malloc(output_table_size);
	uint16_t *clut = malloc(clut_size);
	if (input_table == NULL || output_table == NULL || clut == NULL) {
		free(input_table);
		free(output_table);
		free(clut);
		return false;
	}

	const uint8_t *raw_input_table = &raw[52];
	for (size_t i = 0; i < input_table_len; i++) {
		input_table[i] = parse_u16(&raw_input_table[i * sizeof(uint16_t)]);
	}

	const uint8_t *raw_clut = &raw_input_table[input_table_size];
	for (size_t i = 0; i < clut_len; i++) {
		clut[i] = parse_u16(&raw_clut[i * sizeof(uint16_t)]);
	}

	const uint8_t *raw_output_table = &raw_clut[clut_size];
	for (size_t i = 0; i < output_table_len; i++) {
		output_table[i] = parse_u16(&raw_output_table[i * sizeof(uint16_t)]);
	}

	lut16.input_table = input_table;
	lut16.output_table = output_table;
	lut16.clut = clut;

	*out = lut16;
	return true;
}

static ssize_t
parse_lut_ab_curve(struct icc_lut_ab_curve_priv *out, const uint8_t *raw, size_t size)
{
	if (size < 4) {
		fprintf(stderr, "invalid LUT curve size\n");
		return -1;
	}

	uint32_t type = parse_u32(raw);
	switch (type) {
	case ICC_TYPE_CURVE:
		out->base.curve = &out->curve;
		return parse_curve(&out->curve, raw, size);
	case ICC_TYPE_PARAMETRIC_CURVE:
		out->base.parametric_curve = &out->parametric_curve;
		return parse_parametric_curve(&out->parametric_curve, raw, size);
	default:;
		char type_str[5];
		sig_str(type_str, type);
		fprintf(stderr, "invalid LUT curve type '%s'\n", type_str);
		return -1;
	}
}

static bool
parse_lut_ab_curve_array(const struct icc_lut_ab_curve *out[static 16],
	struct icc_lut_ab_curve_priv out_priv[static 16], uint8_t *out_len,
	const uint8_t *raw, size_t size, size_t offset, uint8_t num_curves)
{
	if (offset == 0) {
		*out = NULL;
		*out_len = 0;
		return true;
	}

	for (size_t i = 0; i < num_curves; i++) {
		if (offset >= size) {
			fprintf(stderr, "curve offset exceeds LUT field size\n");
			return false;
		}

		ssize_t n = parse_lut_ab_curve(&out_priv[i], &raw[offset], size - offset);
		if (n < 0) {
			return false;
		}
		assert(n > 0);
		offset += (size_t) n;
		out[i] = &out_priv[i].base;

		if (offset % 4 != 0) {
			offset += 4 - (offset % 4);
		}
	}

	*out_len = num_curves;
	return true;
}

static bool
parse_lut_ab_matrix(struct icc_s15_fixed16_number matrix_mult[static 9],
	struct icc_s15_fixed16_number matrix_add[static 3], const uint8_t *raw, size_t size)
{
	if (size < 12 * S15_FIXED16_NUMBER_SIZE) {
		fprintf(stderr, "invalid matrix offset\n");
		return false;
	}

	for (size_t i = 0; i < 9; i++) {
		matrix_mult[i] = parse_s15_fixed16_number(&raw[i * S15_FIXED16_NUMBER_SIZE]);
	}
	for (size_t i = 0; i < 3; i++) {
		matrix_add[i] = parse_s15_fixed16_number(&raw[(i + 9) * S15_FIXED16_NUMBER_SIZE]);
	}

	return true;
}

static bool
parse_lut_ab_clut8(struct icc_lut_ab *lut, const uint8_t *raw, size_t size, size_t clut_len)
{
	size_t clut_size = clut_len * sizeof(lut->clut8[0]);
	if (size < clut_size) {
		fprintf(stderr, "invalid CLUT offset\n");
		return false;
	}

	uint8_t *clut8 = malloc(clut_size);
	if (clut8 == NULL) {
		return false;
	}

	memcpy(clut8, raw, clut_size);

	lut->clut8 = clut8;
	return true;
}

static bool
parse_lut_ab_clut16(struct icc_lut_ab *lut, const uint8_t *raw, size_t size, size_t clut_len)
{
	size_t clut_size = clut_len * sizeof(lut->clut16[0]);
	if (size < clut_size) {
		fprintf(stderr, "invalid CLUT offset\n");
		return false;
	}

	uint16_t *clut16 = malloc(clut_size);
	if (clut16 == NULL) {
		return false;
	}

	for (size_t i = 0; i < clut_len; i++) {
		clut16[i] = parse_u16(&raw[i * sizeof(uint16_t)]);
	}

	lut->clut16 = clut16;
	return true;
}

static bool
parse_lut_ab_clut(struct icc_lut_ab *lut, const uint8_t *raw, size_t size)
{
	if (size < 20) {
		fprintf(stderr, "invalid CLUT offset\n");
		return false;
	}

	const uint8_t *num_grid_points = raw;
	uint8_t precision = raw[16];
	const uint8_t *raw_points = &raw[20];
	size_t raw_points_size = size - 20;

	memcpy(lut->clut_num_grid_points, num_grid_points, sizeof(lut->clut_num_grid_points));

	size_t clut_len = lut->num_output_channels;
	assert(lut->num_input_channels <= 16);
	for (size_t i = 0; i < lut->num_input_channels; i++) {
		clut_len *= num_grid_points[i];
	}

	switch (precision) {
	case 0x01:
		return parse_lut_ab_clut8(lut, raw_points, raw_points_size, clut_len);
	case 0x02:
		return parse_lut_ab_clut16(lut, raw_points, raw_points_size, clut_len);
	default:
		fprintf(stderr, "invalid CLUT precision 0x%"PRIX8"\n", precision);
		return false;
	}
}

static bool
parse_lut_ab(struct icc_lut_ab_priv *out, uint32_t type, const uint8_t *raw, size_t size)
{
	if (!expect_size(size, 32, "lutAToB or lutBToA")) {
		return false;
	}

	struct icc_lut_ab lut = {
		.num_input_channels = raw[8],
		.num_output_channels = raw[9],
	};

	if (lut.num_input_channels > 16) {
		fprintf(stderr, "number of input channels exceeds maximum capacity\n");
		return false;
	}
	if (lut.num_output_channels > 16) {
		fprintf(stderr, "number of output channels exceeds maximum capacity\n");
		return false;
	}

	uint32_t b_curves_offset = parse_u32(&raw[12]);
	uint32_t matrix_offset = parse_u32(&raw[16]);
	uint32_t m_curves_offset = parse_u32(&raw[20]);
	uint32_t clut_offset = parse_u32(&raw[24]);
	uint32_t a_curves_offset = parse_u32(&raw[28]);

	if (b_curves_offset >= size || matrix_offset >= size || m_curves_offset >= size ||
			clut_offset >= size || a_curves_offset >= size) {
		fprintf(stderr, "invalid offset\n");
		return false;
	}

	uint8_t a_curves_len, m_curves_len, b_curves_len;
	switch (type) {
	case ICC_TYPE_LUT_ATOB:
		lut.direction = ICC_LUT_AB_DIRECTION_ATOB;
		a_curves_len = lut.num_input_channels;
		m_curves_len = lut.num_output_channels;
		b_curves_len = lut.num_output_channels;
		break;
	case ICC_TYPE_LUT_BTOA:
		lut.direction = ICC_LUT_AB_DIRECTION_BTOA;
		b_curves_len = lut.num_input_channels;
		m_curves_len = lut.num_input_channels;
		a_curves_len = lut.num_output_channels;
		break;
	default:
		abort(); // unreachable
	}

	if (!parse_lut_ab_curve_array(lut.b_curves, out->b_curves, &lut.b_curves_len,
			raw, size, b_curves_offset, b_curves_len)) {
		fprintf(stderr, "failed to parse B curve array\n");
		return false;
	}
	if (matrix_offset > 0) {
		if (!parse_lut_ab_matrix(lut.matrix_mult, lut.matrix_add,
				&raw[matrix_offset], size - matrix_offset)) {
			fprintf(stderr, "failed to parse matrix\n");
			return false;
		}
		lut.has_matrix = true;
	}
	if (!parse_lut_ab_curve_array(lut.m_curves, out->m_curves, &lut.m_curves_len,
			raw, size, m_curves_offset, m_curves_len)) {
		fprintf(stderr, "failed to parse M curve array\n");
		return false;
	}
	if (clut_offset > 0 &&
			!parse_lut_ab_clut(&lut, &raw[clut_offset], size - clut_offset)) {
		fprintf(stderr, "failed to parse CLUT\n");
		return false;
	}
	if (!parse_lut_ab_curve_array(lut.a_curves, out->a_curves, &lut.a_curves_len,
			raw, size, a_curves_offset, a_curves_len)) {
		fprintf(stderr, "failed to parse A curve array\n");
		return false;
	}

	out->base = lut;
	return true;
}

static struct icc_chromaticity_xy
parse_chromaticity_xy(const uint8_t raw[static 8])
{
	return (struct icc_chromaticity_xy) {
		.x = { parse_u32(&raw[0]) },
		.y = { parse_u32(&raw[4]) },
	};
}

static bool
parse_chromaticity(struct icc_chromaticity *out, uint32_t type, const uint8_t *raw, size_t size)
{
	if (!expect_type(type, ICC_TYPE_CHROMATICITY) ||
			!expect_size(size, 36, "chromaticity")) {
		return false;
	}

	uint16_t num_device_channels = parse_u16(&raw[8]);
	uint16_t colorant_type = parse_u16(&raw[10]);

	switch (colorant_type) {
	case ICC_COLORANT_TYPE_UNKNOWN:
	case ICC_COLORANT_TYPE_BT_709:
	case ICC_COLORANT_TYPE_SMPTE:
	case ICC_COLORANT_TYPE_EBU3213:
	case ICC_COLORANT_TYPE_P22:
		break; // ok
	default:
		fprintf(stderr, "invalid phosphor/colorant type %u\n", colorant_type);
		return false;
	}

	struct icc_chromaticity_xy ch1 = parse_chromaticity_xy(&raw[12]);
	struct icc_chromaticity_xy ch2 = parse_chromaticity_xy(&raw[20]);
	struct icc_chromaticity_xy ch3 = parse_chromaticity_xy(&raw[28]);
	// TODO: more channels

	*out = (struct icc_chromaticity) {
		.num_device_channels = num_device_channels,
		.colorant_type = colorant_type,
		.ch1 = ch1,
		.ch2 = ch2,
		.ch3 = ch3,
	};
	return true;
}

static bool
parse_chromatic_adaptation(struct icc_chromatic_adaptation *out, uint32_t type,
	const uint8_t *raw, size_t size)
{
	if (!expect_type(type, ICC_TYPE_S15_FIXED16_ARRAY) ||
			!expect_size(size, 8 + 9 * S15_FIXED16_NUMBER_SIZE, "chromatic adaptation")) {
		return false;
	}

	for (size_t i = 0; i < 9; i++) {
		out->matrix[i] = parse_s15_fixed16_number(&raw[8 + i * S15_FIXED16_NUMBER_SIZE]);
	}

	return true;
}

static bool
parse_dict_str(const char **out, const uint8_t *raw, size_t size,
	uint32_t str_offset, uint32_t str_size, bool localized)
{
	if (str_offset == 0) {
		*out = NULL;
		return true;
	}
	if (str_size == 0) {
		*out = strdup("");
		return *out != NULL;
	}
	if (str_offset + str_size > size) {
		fprintf(stderr, "invalid dict record string offset/size\n");
		return false;
	}

	if (localized) {
		*out = parse_multi_localized_unicode(ICC_TYPE_MULTI_LOCALIZED_UNICODE, &raw[str_offset], str_size);
	} else {
		*out = decode_utf16(&raw[str_offset], str_size);
	}
	return *out != NULL;
}

static bool
parse_dict_record(struct icc_dict_record *out, const uint8_t *raw, size_t size,
	size_t record_offset, uint32_t record_size)
{
	const uint8_t *raw_record = &raw[record_offset];
	uint32_t name_offset = parse_u32(&raw_record[0]);
	uint32_t name_size = parse_u32(&raw_record[4]);
	uint32_t value_offset = parse_u32(&raw_record[8]);
	uint32_t value_size = parse_u32(&raw_record[12]);
	uint32_t display_name_offset = 0;
	uint32_t display_name_size = 0;
	uint32_t display_value_offset = 0;
	uint32_t display_value_size = 0;
	if (record_size >= 24) {
		display_name_offset = parse_u32(&raw_record[16]);
		display_name_size = parse_u32(&raw_record[20]);
	}
	if (record_size >= 32) {
		display_value_offset = parse_u32(&raw_record[24]);
		display_value_size = parse_u32(&raw_record[28]);
	}

	if (name_offset == 0) {
		fprintf(stderr, "dict record name offset cannot be zero\n");
		return false;
	}

	return parse_dict_str(&out->name, raw, size, name_offset, name_size, false) &&
		parse_dict_str(&out->value, raw, size, value_offset, value_size, false) &&
		parse_dict_str(&out->display_name, raw, size, display_name_offset, display_name_size, true) &&
		parse_dict_str(&out->display_value, raw, size, display_value_offset, display_value_size, true);
}

static bool
parse_dict(struct icc_dict *out, uint32_t type, const uint8_t *raw, size_t size)
{
	if (!expect_type(type, ICC_TYPE_DICT) || !expect_size(size, 16, "dict")) {
		return false;
	}

	uint32_t records_len = parse_u32(&raw[8]);
	uint32_t record_size = parse_u32(&raw[12]);
	switch (record_size) {
	case 16:
	case 24:
	case 32:
		break; // ok
	default:
		fprintf(stderr, "invalid dict record size %u\n", record_size);
		return false;
	}

	if (!expect_size(size, 16 + records_len * record_size, "dict")) {
		return false;
	}

	struct icc_dict_record *records = NULL;
	if (records_len > 0) {
		records = calloc(records_len, sizeof(records[0]));
		if (records == NULL) {
			return false;
		}
	}

	for (size_t i = 0; i < records_len; i++) {
		if (!parse_dict_record(&records[i], raw, size, 16 + i * record_size, record_size)) {
			return false;
		}
	}

	*out = (struct icc_dict) {
		.len = records_len,
		.records = records,
	};
	return true;
}

static ssize_t
parse_multi_process_curve_segment(struct icc_multi_process_curve_segment_priv *out,
	const uint8_t *raw, size_t size)
{
	size_t header_size = 12;
	if (size < header_size) {
		fprintf(stderr, "invalid multi process curve segment size\n");
		return -1;
	}

	uint32_t type = parse_u32(&raw[0]);
	struct icc_multi_process_curve_segment segment = {
		.type = type,
	};
	size_t total_size;
	switch (type) {
	case ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_FORMULA:;
		struct icc_multi_process_curve_formula *formula = &out->formula;
		segment.formula = formula;

		uint16_t formula_type = parse_u16(&raw[8]);
		float *params[8] = {0};
		size_t params_len = 0;
		switch (formula_type) {
		case ICC_MULTI_PROCESS_CURVE_FORMULA_TYPE_0:
			params[params_len++] = &formula->gamma;
			params[params_len++] = &formula->a;
			params[params_len++] = &formula->b;
			params[params_len++] = &formula->c;
			break;
		case ICC_MULTI_PROCESS_CURVE_FORMULA_TYPE_1:
			params[params_len++] = &formula->gamma;
			params[params_len++] = &formula->a;
			params[params_len++] = &formula->b;
			params[params_len++] = &formula->c;
			params[params_len++] = &formula->d;
			break;
		case ICC_MULTI_PROCESS_CURVE_FORMULA_TYPE_2:
			params[params_len++] = &formula->a;
			params[params_len++] = &formula->b;
			params[params_len++] = &formula->c;
			params[params_len++] = &formula->d;
			params[params_len++] = &formula->e;
			break;
		default:
			fprintf(stderr, "invalid multi process curve segment formula type %u\n", formula_type);
			return -1;
		}
		assert(params_len <= sizeof(params) / sizeof(params[0]));

		total_size = header_size + params_len * FLOAT32_NUMBER_SIZE;
		if (size < total_size) {
			fprintf(stderr, "invalid multi process curve segment formula size\n");
			return -1;
		}

		formula->type = formula_type;
		for (size_t i = 0; i < params_len; i++) {
			*params[i] = parse_float32(&raw[header_size + i * FLOAT32_NUMBER_SIZE]);
		}
		break;
	case ICC_MULTI_PROCESS_CURVE_SEGMENT_TYPE_SAMPLED:;
		struct icc_multi_process_curve_sampled *sampled = &out->sampled;
		segment.sampled = sampled;

		uint32_t sampled_len = parse_u32(&raw[8]);
		total_size = header_size + sampled_len * FLOAT32_NUMBER_SIZE;
		if (size < total_size) {
			fprintf(stderr, "invalid multi process curve segment sampled size\n");
			return -1;
		}

		float *entries = calloc(sampled_len, sizeof(entries[0]));
		if (entries == NULL) {
			return -1;
		}

		for (size_t i = 0; i < sampled_len; i++) {
			entries[i] = parse_float32(&raw[header_size + i * FLOAT32_NUMBER_SIZE]);
		}

		sampled->len = sampled_len;
		sampled->entries = entries;
		break;
	default:;
		char type_str[5];
		sig_str(type_str, type);
		fprintf(stderr, "unknown multi process curve segment type '%s'\n", type_str);
		return -1;
	}

	out->base = segment;
	return (ssize_t) total_size;
}

static bool
parse_multi_process_curve(struct icc_multi_process_curve_priv *out,
	const uint8_t *raw, size_t size)
{
	size_t header_size = 12;
	if (size < header_size) {
		fprintf(stderr, "invalid multi process curve size\n");
		return false;
	}

	uint32_t type = parse_u32(&raw[0]);
	if (type != ICC_SIG('c', 'u', 'r', 'f')) {
		fprintf(stderr, "invalid multi process curve type\n");
		return false;
	}

	struct icc_multi_process_curve curve = {
		.num_segments = parse_u16(&raw[8]),
	};
	if (curve.num_segments == 0) {
		fprintf(stderr, "multi process curve must contain at least one segment\n");
		return false;
	}

	size_t break_points_size = (size_t) FLOAT32_NUMBER_SIZE * (curve.num_segments - 1);
	if (size < header_size + break_points_size) {
		fprintf(stderr, "invalid multi process curve size\n");
		return false;
	}

	float *break_points = NULL;
	if (curve.num_segments > 1) {
		break_points = calloc(curve.num_segments - 1, sizeof(break_points[0]));
		if (break_points == NULL) {
			return false;
		}
	}

	struct icc_multi_process_curve_segment_priv *segments = calloc(curve.num_segments, sizeof(segments[0]));
	const struct icc_multi_process_curve_segment **segment_ptrs = calloc(curve.num_segments, sizeof(segment_ptrs[0]));
	if (segments == NULL || segment_ptrs == NULL) {
		goto error;
	}

	for (uint16_t i = 0; i < curve.num_segments - 1; i++) {
		break_points[i] = parse_float32(&raw[header_size + i * FLOAT32_NUMBER_SIZE]);
	}

	size_t offset = header_size + break_points_size;
	for (size_t i = 0; i < curve.num_segments; i++) {
		ssize_t n = parse_multi_process_curve_segment(&segments[i], &raw[offset], size - offset);
		if (n < 0) {
			goto error;
		}
		assert(n > 0);
		offset += (size_t) n;
		segment_ptrs[i] = &segments[i].base;
	}

	curve.break_points = break_points;
	curve.segments = segment_ptrs;
	*out = (struct icc_multi_process_curve_priv) {
		.base = curve,
		.segments = segments,
	};
	return true;

error:
	free(segments);
	free(segment_ptrs);
	free(break_points);
	return false;
}

static bool
parse_multi_process_curve_set(struct icc_multi_process_element_priv *priv,
	const uint8_t *raw, size_t size)
{
	struct icc_multi_process_element *elem = &priv->base;

	if (elem->num_input_channels != elem->num_output_channels) {
		fprintf(stderr, "number of input channels must be equal to number of output channels in multi process curve set\n");
		return false;
	}

	size_t table_entry_size = 2 * sizeof(uint32_t);
	size_t table_size = elem->num_input_channels * table_entry_size;
	if (size < 12 + table_size) {
		fprintf(stderr, "invalid multi process curve set size\n");
		return false;
	}

	struct icc_multi_process_curve_priv *curves = calloc(elem->num_input_channels, sizeof(curves[0]));
	const struct icc_multi_process_curve **curve_ptrs = calloc(elem->num_input_channels, sizeof(curve_ptrs[0]));
	if (curves == NULL || curve_ptrs == NULL) {
		goto error;
	}

	for (size_t i = 0; i < elem->num_input_channels; i++) {
		const uint8_t *raw_table_entry = &raw[12 + i * table_entry_size];
		uint32_t curve_offset = parse_u32(&raw_table_entry[0]);
		uint32_t curve_size = parse_u32(&raw_table_entry[4]);
		if (curve_offset + curve_size > size) {
			fprintf(stderr, "invalid multi process curve set table curve\n");
			goto error;
		}

		if (!parse_multi_process_curve(&curves[i], &raw[curve_offset], curve_size)) {
			goto error;
		}

		curve_ptrs[i] = &curves[i].base;
	}

	priv->curves = curves;
	elem->curves = curve_ptrs;
	return true;

error:
	free(curves);
	free(curve_ptrs);
	return false;
}

static bool
parse_multi_process_matrix(struct icc_multi_process_element_priv *priv,
	const uint8_t *raw, size_t size)
{
	struct icc_multi_process_element *elem = &priv->base;

	size_t mult_len = elem->num_input_channels * elem->num_output_channels;
	size_t add_len = elem->num_output_channels;
	if (size < 12 + (mult_len + add_len) * FLOAT32_NUMBER_SIZE) {
		fprintf(stderr, "invalid multi process matrix size\n");
		return false;
	}

	float *mult = calloc(mult_len, sizeof(mult[0]));
	float *add = calloc(add_len, sizeof(add[0]));
	if (mult == NULL || add == NULL) {
		free(mult);
		free(add);
		return false;
	}

	for (size_t i = 0; i < mult_len; i++) {
		mult[i] = parse_float32(&raw[12 + i * FLOAT32_NUMBER_SIZE]);
	}
	for (size_t i = 0; i < add_len; i++) {
		add[i] = parse_float32(&raw[12 + (mult_len + i) * FLOAT32_NUMBER_SIZE]);
	}

	elem->matrix_mult = mult;
	elem->matrix_add = add;
	return true;
}

static bool
parse_multi_process_clut(struct icc_multi_process_element_priv *priv,
	const uint8_t *raw, size_t size)
{
	struct icc_multi_process_element *elem = &priv->base;

	if (size < 28) {
		fprintf(stderr, "invalid multi process CLUT size\n");
		return false;
	}

	const uint8_t *raw_num_grid_points = &raw[12];
	size_t clut_len = elem->num_output_channels;
	for (size_t i = 0; i < elem->num_input_channels; i++) {
		clut_len *= raw_num_grid_points[i];
	}
	if (clut_len == 0) {
		fprintf(stderr, "invalid number of grid points for multi process CLUT\n");
		return false;
	}

	if (size < 28 + clut_len * FLOAT32_NUMBER_SIZE) {
		fprintf(stderr, "invalid multi process CLUT size\n");
		return false;
	}

	float *data_points = calloc(clut_len, sizeof(data_points[0]));
	if (data_points == NULL) {
		return false;
	}

	for (size_t i = 0; i < clut_len; i++) {
		data_points[i] = parse_float32(&raw[28 + i * FLOAT32_NUMBER_SIZE]);
	}

	priv->base.clut = &priv->clut;
	priv->clut.data_points = data_points;
	memcpy(priv->clut.num_grid_points, raw_num_grid_points, elem->num_input_channels);
	return true;
}

static bool
parse_multi_process_element(struct icc_multi_process_element_priv *out,
	const uint8_t *raw, size_t size)
{
	if (size < 12) {
		fprintf(stderr, "invalid multi process element size\n");
		return false;
	}

	uint32_t type = parse_u32(&raw[0]);
	out->base = (struct icc_multi_process_element) {
		.type = type,
		.num_input_channels = parse_u16(&raw[8]),
		.num_output_channels = parse_u16(&raw[10]),
	};

	switch (type) {
	case ICC_MULTI_PROCESS_ELEMENT_TYPE_CURVE_SET:
		return parse_multi_process_curve_set(out, raw, size);
	case ICC_MULTI_PROCESS_ELEMENT_TYPE_MATRIX:
		return parse_multi_process_matrix(out, raw, size);
	case ICC_MULTI_PROCESS_ELEMENT_TYPE_CLUT:
		return parse_multi_process_clut(out, raw, size);
	default:;
		// TODO: ignore 'bACS' and 'eACS'
		char type_str[5];
		sig_str(type_str, type);
		fprintf(stderr, "invalid multi process element type '%s'\n", type_str);
		return false;
	}
}

static bool
parse_multi_process_transform(struct icc_multi_process_transform_priv *out,
	uint32_t type, const uint8_t *raw, size_t size)
{
	size_t header_size = 16;
	if (!expect_type(type, ICC_TYPE_MULTI_PROCESS_ELEMENTS) ||
			!expect_size(size, header_size, "multiProcessElements")) {
		return false;
	}

	struct icc_multi_process_transform transform = {
		.num_input_channels = parse_u16(&raw[8]),
		.num_output_channels = parse_u16(&raw[10]),
		.len = parse_u32(&raw[12]),
	};

	if (transform.len == 0) {
		fprintf(stderr, "multiProcessElements length must be greater than zero\n");
		return false;
	}

	size_t table_entry_size = 2 * sizeof(uint32_t);
	size_t table_size = transform.len * table_entry_size;
	if (!expect_size(size, header_size + table_size, "multiProcessElements")) {
		return false;
	}

	struct icc_multi_process_element_priv *elems = calloc(transform.len, sizeof(elems[0]));
	const struct icc_multi_process_element **elem_ptrs = calloc(transform.len, sizeof(elem_ptrs[0]));
	if (elems == NULL || elem_ptrs == NULL) {
		goto error;
	}

	for (size_t i = 0; i < transform.len; i++) {
		const uint8_t *raw_table_entry = &raw[header_size + i * table_entry_size];
		uint32_t elem_offset = parse_u32(&raw_table_entry[0]);
		uint32_t elem_size = parse_u32(&raw_table_entry[4]);
		if (elem_offset + elem_size > size) {
			fprintf(stderr, "invalid multiProcessElements table entry\n");
			goto error;
		}

		if (!parse_multi_process_element(&elems[i], &raw[elem_offset], elem_size)) {
			goto error;
		}

		elem_ptrs[i] = &elems[i].base;
	}

	transform.elements = elem_ptrs;
	out->base = transform;
	out->elements = elems;
	return true;

error:
	free(elems);
	free(elem_ptrs);
	return false;
}

static bool
parse_element_data(struct icc_profile *profile, struct icc_element *elem,
	uint32_t type, const uint8_t *raw, size_t size)
{
	switch (elem->tag) {
	case ICC_TAG_CHAR_TARGET:
		elem->text = parse_text(type, raw, size);
		return elem->text != NULL;
	case ICC_TAG_COPYRIGHT:
		if (profile->header.major_version >= 4) {
			elem->text = parse_multi_localized_unicode(type, raw, size);
		} else {
			elem->text = parse_text(type, raw, size);
		}
		return elem->text != NULL;
	case ICC_TAG_PROFILE_DESCRIPTION:
	case ICC_TAG_DEVICE_MFG_DESC:
	case ICC_TAG_DEVICE_MODEL_DESC:
	case ICC_TAG_SCREENING_DESC:
	case ICC_TAG_VIEWING_COND_DESC:
		if (profile->header.major_version >= 4) {
			elem->text_description = parse_multi_localized_unicode(type, raw, size);
		} else {
			elem->text_description = parse_text_description(type, raw, size);
		}
		return elem->text_description != NULL;
	case ICC_TAG_LUMINANCE:
	case ICC_TAG_MEDIA_BLACK_POINT:
	case ICC_TAG_MEDIA_WHITE_POINT:
	case ICC_TAG_RED_MATRIX_COLUMN:
	case ICC_TAG_GREEN_MATRIX_COLUMN:
	case ICC_TAG_BLUE_MATRIX_COLUMN:
		return parse_xyz(&elem->xyz, type, raw, size);
	case ICC_TAG_RED_TRC:
	case ICC_TAG_GREEN_TRC:
	case ICC_TAG_BLUE_TRC:
	case ICC_TAG_GRAY_TRC:
		switch (type) {
		case ICC_TYPE_CURVE:
			return parse_curve(&elem->curve, raw, size) >= 0;
		case ICC_TYPE_PARAMETRIC_CURVE:
			return parse_parametric_curve(&elem->parametric_curve, raw, size) >= 0;
		default:;
			char type_str[5];
			sig_str(type_str, type);
			fprintf(stderr, "invalid type: got '%s', want 'curv' or 'para'\n", type_str);
			return false;
		}
	case ICC_TAG_ATOB0:
	case ICC_TAG_ATOB1:
	case ICC_TAG_ATOB2:
		switch (type) {
		case ICC_TYPE_LUT8:
			return parse_lut8(&elem->lut8, raw, size);
		case ICC_TYPE_LUT16:
			return parse_lut16(&elem->lut16, raw, size);
		case ICC_TYPE_LUT_ATOB:
			return parse_lut_ab(&elem->lut_ab, type, raw, size);
		default:;
			char type_str[5];
			sig_str(type_str, type);
			fprintf(stderr, "invalid type: got '%s', want 'mft1', 'mft2' or 'mAB '\n", type_str);
			return false;
		}
		break;
	case ICC_TAG_BTOA0:
	case ICC_TAG_BTOA1:
	case ICC_TAG_BTOA2:
	case ICC_TAG_GAMUT:
	case ICC_TAG_PREVIEW0:
	case ICC_TAG_PREVIEW1:
	case ICC_TAG_PREVIEW2:
		switch (type) {
		case ICC_TYPE_LUT8:
			return parse_lut8(&elem->lut8, raw, size);
		case ICC_TYPE_LUT16:
			return parse_lut16(&elem->lut16, raw, size);
		case ICC_TYPE_LUT_BTOA:
			return parse_lut_ab(&elem->lut_ab, type, raw, size);
		default:;
			char type_str[5];
			sig_str(type_str, type);
			fprintf(stderr, "invalid type: got '%s', want 'mft1', 'mft2' or 'mBA '\n", type_str);
			return false;
		}
		break;
	case ICC_TAG_CHROMATICITY:
		return parse_chromaticity(&elem->chromaticity, type, raw, size);
	case ICC_TAG_CHROMATIC_ADAPTATION:
		return parse_chromatic_adaptation(&elem->chromatic_adaptation, type, raw, size);
	case ICC_TAG_METADATA:
		return parse_dict(&elem->metadata, type, raw, size);
	case ICC_TAG_PERCEPTUAL_RENDERINT_INTENT_GAMUT:;
		uint32_t sig;
		if (!parse_sig(&sig, type, raw, size)) {
			return false;
		}
		switch (sig) {
		case ICC_REF_MEDIUM_GAMUT_PERCEPTUAL:
			elem->perceptual_rendering_gamut = sig;
			return true;
		default:
			return false;
		}
		break;
	case ICC_TAG_BTOD0:
	case ICC_TAG_BTOD1:
	case ICC_TAG_BTOD2:
	case ICC_TAG_BTOD3:
	case ICC_TAG_DTOB0:
	case ICC_TAG_DTOB1:
	case ICC_TAG_DTOB2:
	case ICC_TAG_DTOB3:
		return parse_multi_process_transform(&elem->multi_process_transform,
			type, raw, size);
	case ICC_TAG_CALIBRATION_DATE_TIME:
	case ICC_TAG_CRD_INFO:
	case ICC_TAG_DEVICE_SETTINGS:
	case ICC_TAG_MEASUREMENT:
	case ICC_TAG_NAMED_COLOR:
	case ICC_TAG_NAMED_COLOR2:
	case ICC_TAG_OUTPUT_RESPONSE:
	case ICC_TAG_PROFILE_SEQUENCE_DESC:
	case ICC_TAG_PS2_CRD0:
	case ICC_TAG_PS2_CRD1:
	case ICC_TAG_PS2_CRD2:
	case ICC_TAG_PS2_CRD3:
	case ICC_TAG_PS2_CSA:
	case ICC_TAG_PS2_RENDERING_INTENT:
	case ICC_TAG_SCREENING:
	case ICC_TAG_TECHNOLOGY:
	case ICC_TAG_UCRBG:
	case ICC_TAG_VIEWING_CONDITIONS:
	case ICC_TAG_CICP:
	case ICC_TAG_COLORANT_ORDER:
	case ICC_TAG_COLORANT_TABLE:
	case ICC_TAG_COLORANT_TABLE_OUT:
	case ICC_TAG_COLORIMETRIC_INTENT_IMAGE_STATE:
	case ICC_TAG_PROFILE_SEQUENCE_IDENTIFIER:
	case ICC_TAG_SATURATION_RENDERING_INTENT_GAMUT:
		return true; // TODO
	}
	abort(); // unreachable
}

static bool
parse_data(struct icc_profile *profile, const uint8_t *raw, size_t size)
{
	for (size_t i = 0; profile->element_ptrs[i] != NULL; i++) {
		struct icc_element *elem = profile->element_ptrs[i];

		assert(elem->offset >= HEADER_SIZE + profile->tag_table_size);
		size_t offset = elem->offset - (HEADER_SIZE + profile->tag_table_size);
		assert(offset + elem->size <= size);
		const uint8_t *elem_data = &raw[offset];

		if (elem->size < 4) {
			fprintf(stderr, "invalid element size (shorter than type field)\n");
			return false;
		}

		elem->type = parse_u32(elem_data);
		if (!parse_element_data(profile, elem, elem->type, elem_data, elem->size)) {
			char tag_str[5];
			sig_str(tag_str, elem->tag);
			fprintf(stderr, "failed to parse element '%s'\n", tag_str);
			return false;
		}
	}

	return true;
}

struct icc_profile *
icc_profile_parse(FILE *f)
{
	struct icc_profile *profile = calloc(1, sizeof(*profile));
	if (profile == NULL) {
		return NULL;
	}

	if (!read_header(profile, f)) {
		goto error;
	}
	if (!read_tag_table(profile, f)) {
		goto error;
	}

	size_t raw_data_size = profile->size - HEADER_SIZE - profile->tag_table_size;
	uint8_t *raw_data = malloc(raw_data_size);
	if (raw_data == NULL) {
		goto error;
	}
	if (!read_full(f, raw_data, raw_data_size) || !parse_data(profile, raw_data, raw_data_size)) {
		free(raw_data);
		goto error;
	}
	free(raw_data);

	return profile;

error:
	icc_profile_destroy(profile);
	return NULL;
}

static void
dict_finish(struct icc_dict *dict) {
	struct icc_dict_record *records = (struct icc_dict_record *) dict->records;
	for (size_t i = 0; i < dict->len; i++) {
		struct icc_dict_record *record = &records[i];
		free((char *) record->name);
		free((char *) record->value);
		free((char *) record->display_name);
		free((char *) record->display_value);
	}
	free(records);
}

static void
multi_process_curve_finish(struct icc_multi_process_curve_priv *priv)
{
	struct icc_multi_process_curve *curve = &priv->base;
	for (size_t i = 0; i < curve->num_segments; i++) {
		free((float *) priv->segments[i].sampled.entries);
	}
	free(priv->segments);
	free((const struct icc_multi_process_curve_segment **) curve->segments);
	free((float *) curve->break_points);
}

static void
multi_process_element_finish(struct icc_multi_process_element_priv *priv)
{
	struct icc_multi_process_element *elem = &priv->base;
	if (priv->curves != NULL) {
		for (size_t i = 0; i < elem->num_input_channels; i++) {
			multi_process_curve_finish(&priv->curves[i]);
		}
		free((const struct icc_multi_process_curve **) elem->curves);
		free(priv->curves);
	}
	free((float *) elem->matrix_mult);
	free((float *) elem->matrix_add);
	free((float *) priv->clut.data_points);
}

static void
finish_ab_curve_array(const struct icc_lut_ab_curve_priv curves[static 16])
{
	for (size_t i = 0; i < 16; i++) {
		free((uint16_t *) curves[i].curve.values);
	}
}

static void
element_finish(struct icc_element *elem)
{
	free(elem->text);
	free(elem->text_description);
	free((uint16_t *) elem->curve.values);
	free((uint8_t *) elem->lut8.input_table);
	free((uint8_t *) elem->lut8.clut);
	free((uint8_t *) elem->lut8.output_table);
	free((uint16_t *) elem->lut16.input_table);
	free((uint16_t *) elem->lut16.clut);
	free((uint16_t *) elem->lut16.output_table);
	dict_finish(&elem->metadata);
	for (size_t i = 0; i < elem->multi_process_transform.base.len; i++) {
		multi_process_element_finish(&elem->multi_process_transform.elements[i]);
	}
	free((const struct icc_multi_process_element **) elem->multi_process_transform.base.elements);
	free(elem->multi_process_transform.elements);
	finish_ab_curve_array(elem->lut_ab.a_curves);
	free((uint8_t *) elem->lut_ab.base.clut8);
	free((uint16_t *) elem->lut_ab.base.clut16);
	finish_ab_curve_array(elem->lut_ab.m_curves);
	finish_ab_curve_array(elem->lut_ab.b_curves);
}

void
icc_profile_destroy(struct icc_profile *profile)
{
	if (profile->element_ptrs != NULL) {
		for (size_t i = 0; profile->element_ptrs[i] != NULL; i++) {
			element_finish(profile->element_ptrs[i]);
		}
	}
	free(profile->element_ptrs);
	free(profile->elements);
	free(profile);
}

const struct icc_header *
icc_profile_get_header(const struct icc_profile *profile)
{
	return &profile->header;
}

const struct icc_element *const *
icc_profile_get_elements(const struct icc_profile *profile)
{
	return (const struct icc_element *const *) profile->element_ptrs;
}

enum icc_tag
icc_element_get_tag(const struct icc_element *elem)
{
	return elem->tag;
}

const char *
icc_element_get_char_target(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_CHAR_TARGET);
	return elem->text;
}

const char *
icc_element_get_copyright(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_COPYRIGHT);
	return elem->text;
}

const char *
icc_element_get_profile_description(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_PROFILE_DESCRIPTION);
	return elem->text_description;
}

const char *
icc_element_get_device_mfg_desc(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_DEVICE_MFG_DESC);
	return elem->text_description;
}

const char *
icc_element_get_device_model_desc(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_DEVICE_MODEL_DESC);
	return elem->text_description;
}

const char *
icc_element_get_screening_desc(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_SCREENING_DESC);
	return elem->text_description;
}

const char *
icc_element_get_viewing_cond_desc(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_VIEWING_COND_DESC);
	return elem->text_description;
}

struct icc_s15_fixed16_number
icc_element_get_luminance(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_LUMINANCE);
	return elem->xyz.y; // other channels are ignored
}

struct icc_xyz
icc_element_get_media_black_point(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_MEDIA_BLACK_POINT);
	return elem->xyz;
}

struct icc_xyz
icc_element_get_media_white_point(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_MEDIA_WHITE_POINT);
	return elem->xyz;
}

struct icc_xyz
icc_element_get_red_colorant(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_RED_MATRIX_COLUMN);
	return elem->xyz;
}

struct icc_xyz
icc_element_get_green_colorant(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_GREEN_MATRIX_COLUMN);
	return elem->xyz;
}

struct icc_xyz
icc_element_get_blue_colorant(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_BLUE_MATRIX_COLUMN);
	return elem->xyz;
}

const struct icc_curve *
icc_element_get_curve(const struct icc_element *elem)
{
	switch (elem->tag) {
	case ICC_TAG_RED_TRC:
	case ICC_TAG_GREEN_TRC:
	case ICC_TAG_BLUE_TRC:
	case ICC_TAG_GRAY_TRC:
		break; // ok
	default:
		assert(0);
	}
	return elem->type == ICC_TYPE_CURVE ? &elem->curve : NULL;
}

const struct icc_parametric_curve *
icc_element_get_parametric_curve(const struct icc_element *elem)
{
	switch (elem->tag) {
	case ICC_TAG_RED_TRC:
	case ICC_TAG_GREEN_TRC:
	case ICC_TAG_BLUE_TRC:
	case ICC_TAG_GRAY_TRC:
		break; // ok
	default:
		assert(0);
	}
	return elem->type == ICC_TYPE_PARAMETRIC_CURVE ? &elem->parametric_curve : NULL;
}

const struct icc_lut8 *
icc_element_get_lut8(const struct icc_element *elem)
{
	switch (elem->tag) {
	case ICC_TAG_ATOB0:
	case ICC_TAG_ATOB1:
	case ICC_TAG_ATOB2:
	case ICC_TAG_BTOA0:
	case ICC_TAG_BTOA1:
	case ICC_TAG_BTOA2:
	case ICC_TAG_GAMUT:
	case ICC_TAG_PREVIEW0:
	case ICC_TAG_PREVIEW1:
	case ICC_TAG_PREVIEW2:
		break; // ok
	default:
		assert(0);
	}
	return elem->type == ICC_TYPE_LUT8 ? &elem->lut8 : NULL;
}

const struct icc_lut16 *
icc_element_get_lut16(const struct icc_element *elem)
{
	switch (elem->tag) {
	case ICC_TAG_ATOB0:
	case ICC_TAG_ATOB1:
	case ICC_TAG_ATOB2:
	case ICC_TAG_BTOA0:
	case ICC_TAG_BTOA1:
	case ICC_TAG_BTOA2:
	case ICC_TAG_GAMUT:
	case ICC_TAG_PREVIEW0:
	case ICC_TAG_PREVIEW1:
	case ICC_TAG_PREVIEW2:
		break; // ok
	default:
		assert(0);
	}
	return elem->type == ICC_TYPE_LUT16 ? &elem->lut16 : NULL;
}

const struct icc_lut_ab *
icc_element_get_lut_ab(const struct icc_element *elem)
{
	switch (elem->tag) {
	case ICC_TAG_ATOB0:
	case ICC_TAG_ATOB1:
	case ICC_TAG_ATOB2:
	case ICC_TAG_BTOA0:
	case ICC_TAG_BTOA1:
	case ICC_TAG_BTOA2:
	case ICC_TAG_GAMUT:
	case ICC_TAG_PREVIEW0:
	case ICC_TAG_PREVIEW1:
	case ICC_TAG_PREVIEW2:
		break; // ok
	default:
		assert(0);
	}
	switch (elem->type) {
	case ICC_TYPE_LUT_ATOB:
	case ICC_TYPE_LUT_BTOA:
		return &elem->lut_ab.base;
	default:
		return NULL;
	}
}

const struct icc_chromaticity *
icc_element_get_chromaticity(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_CHROMATICITY);
	return &elem->chromaticity;
}

const struct icc_chromatic_adaptation *
icc_element_get_chromatic_adaptation(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_CHROMATIC_ADAPTATION);
	return &elem->chromatic_adaptation;
}

struct icc_dict
icc_element_get_metadata(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_METADATA);
	return elem->metadata;
}

enum icc_ref_medium_gamut
icc_element_get_perceptual_rendering_gamut(const struct icc_element *elem)
{
	assert(elem->tag == ICC_TAG_PERCEPTUAL_RENDERINT_INTENT_GAMUT);
	return elem->perceptual_rendering_gamut;
}

const struct icc_multi_process_transform *
icc_element_get_multi_process_transform(const struct icc_element *elem)
{
	switch (elem->tag) {
	case ICC_TAG_BTOD0:
	case ICC_TAG_BTOD1:
	case ICC_TAG_BTOD2:
	case ICC_TAG_BTOD3:
	case ICC_TAG_DTOB0:
	case ICC_TAG_DTOB1:
	case ICC_TAG_DTOB2:
	case ICC_TAG_DTOB3:
		break; // ok
	default:
		assert(0);
	}
	return &elem->multi_process_transform.base;
}
